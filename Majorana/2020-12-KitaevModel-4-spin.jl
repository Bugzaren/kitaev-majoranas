include("src/MajoranaReps.jl")

#include("src/TestMajoranas.jl")


using PyPlot
using Test
using LinearAlgebra
close("all")

function GetEvals(Ham,NumEvs)
    RawEvals = eigvals(Ham)
    PhysEnergies = sortperm(abs.(RawEvals))[end-NumEvs+1:end]
    return sort(RawEvals[PhysEnergies])
end


function MakeNumHam(OpHam,BasisList)
    HSSize = length(BasisList)
    NumericHam = fill(0.0im,HSSize,HSSize)
    for i in 1:HSSize
        for j in 1:HSSize
            NumericHam[i,j]=OpInnerProd(BasisList[j],OpHam,BasisList[i])
        end
    end
    return NumericHam
end

function CompareSpinOpMat(SMat,OpMat,BasisList;title="")
    if title != ""
        println("Comparing $title")
    end
    NOpMat = MakeNumHam(OpMat,BasisList)
    ###Check that the numerical Hamiltonian is equivalent to the 
    
    if !all(isapprox.(NOpMat,SMat,atol=sqrt(eps())))
        figure(figsize=(12,5))
        subplot(1,3,1)
        imshow(abs.(NOpMat))
        subplot(1,3,2)
        imshow(isapprox.(NOpMat,SMat))
        subplot(1,3,3)
        imshow(abs.(SMat))
        suptitle(title)
        error("Error while testing for $title")
    end
end

NumSites = 4
NumPlaques=3


###Define the spin operators
sx = [[0,1] [1,0]]
sy = [[0,-im] [im,0]]
sz = [[1,0] [0,-1]]
II  = [[1,0] [0,1]]
I4  =  diagm(0=>ones(2^NumSites))

MSx(n) = MSpin("x",n)
MSy(n) = MSpin("y",n)
MSz(n) = MSpin("z",n)
    
function MSpin(st,n)
    if st=="x"
        op=sx
    elseif st=="y"
        op=sy
    elseif st=="z"
        op=sz
    else
        error("unknown spin $st")
    end
    if n==1
        kron(op, II, II, II);
    elseif n==2
        kron(II, op, II, II);
    elseif n==3
        kron(II, II, op, II);
    elseif n==4
        kron(II, II, II,op);
    else
        error("unknown spin place")
    end
end



###This is the basis crated by taking Kroenecker deltas
BasisList = kron([Sx(1),1],[Sx(2),1],[Sx(3),1],
                 [Sx(4),1]).*Ket()


for n in 1:NumSites
    CompareSpinOpMat(MSx(n),Sx(n),BasisList;title="Sx($n)")
    CompareSpinOpMat(MSy(n),Sy(n),BasisList;title="Sy($n)")
    CompareSpinOpMat(MSz(n),Sz(n),BasisList;title="Sz($n)")
end


###Here is the hamitlonian
# (H = -J12*ss12 - J24y*ss24y - J24z*ss24z - J34z*ss34z - J13y*ss13y - 
#     J13p*ss13);


###Here we implement the 4-site spin model with couplings
###                 (1) -- x -- (2)
###                /   \       /   \ 
###                y   z       y   z
###                \   /       \   /
###                 (3) -- x -- (4)


###Define the many body spin couplings
ss12 = MSx(1)*MSx(2)
ss24z = MSz(2)*MSz(4)
ss24y = MSy(2)*MSy(4)
ss13z = MSz(1)*MSz(3)
ss13y = MSy(1)*MSy(3)
ss34 = MSx(3)*MSx(4)


###Define the corresponding majorana operators
SM12 = Sx(1)*Sx(2)
SM24z = Sz(2)*Sz(4)
SM24y = Sy(2)*Sy(4)
SM13z = Sz(1)*Sz(3)
SM13y = Sy(1)*Sy(3)
SM34 = Sx(3)*Sx(4)



####Here we assign values to the bond variables
BondValList=[1,1,1,1,1,1] ###For now we take them all to be positive, but his will change later

####The hamiltonian and it's couplings
SpinSpinList = [ss12,ss24z,ss24y,ss13z,ss13y,ss34]
SpinMajoList = [SM12,SM24z,SM24y,SM13z,SM13y,SM34]
SpinCCList = [im*c(1)*c(2),im*c(2)*c(4),im*c(2)*c(4),im*c(1)*c(3),
              im*c(1)*c(3),im*c(3)*c(4)]
CouplingList = [ -randn() for x in SpinSpinList]
Hspin = sum(SpinSpinList.*CouplingList)
Hmaj = sum(SpinMajoList.*CouplingList)
HCC = sum(-SpinCCList.*BondValList.*CouplingList)




###We introduce the parity operators
Dlist = [ParityOp(x) for x in 1:NumSites]
println("Dlist:")
display(Dlist)

##Further we introduce the projectors to the Physical space
Plist = [(1//2)*(1 + ParityOp(x)) for x in 1:NumSites]
println("Plist:")
display(Plist)


##The combined projector is 
FullProj = prod(Plist)
println("FullProj:")
display(FullProj)


####We also have the bond operators
println("Create the bond operators")
BondOpList = [im*bx(1)*bx(2),
              im*bz(2)*bz(4),
              im*by(2)*by(4),
              im*bz(1)*bz(3),
              im*by(1)*by(3),
              im*bx(3)*bx(4)]

NumBonds = length(BondValList)


println("Check commutators:")
for i in 1:NumBonds
    for j in 1:NumBonds
        @test OpComutes(BondOpList[i],BondOpList[j])
    end
end


##We can create a projector for this configuration as
println("Create the bond projectors")
BondProjList =[1//2 + 1//2*BondValList[indx]*BondOpList[indx] for indx in 1:NumBonds]
##The Full Bon Projector Is then
#FullBondProj = prod(BondProjList)
#@test FullProj*FullProj == FullProj



##We can create a projector for this configuration as

LambdaL=BondValList[4]*BondValList[5]
LambdaO=BondValList[1]*BondValList[2]*BondValList[4]*BondValList[6]
LambdaR=BondValList[2]*BondValList[3]


OpLambdaL=BondOpList[4]*BondOpList[5]
OpLambdaO=BondOpList[1]*BondOpList[2]*BondOpList[4]*BondOpList[6]
OpLambdaR=BondOpList[2]*BondOpList[3]


SpinLambdaL=MSy(1)*MSy(3)*MSz(3)*MSz(1)
SpinLambdaO=MSx(1)*MSx(2)*MSz(2)*MSz(4)*MSx(4)*MSx(3)*MSz(3)*MSz(1)
SpinLambdaR=MSy(2)*MSy(4)*MSz(4)*MSz(2)


CompareSpinOpMat(Hspin,Hmaj,BasisList;title="Unporjected Matrix")   

CompareSpinOpMat(SpinLambdaL,OpLambdaL,BasisList;title="Lambda L")   
CompareSpinOpMat(SpinLambdaO,OpLambdaO,BasisList;title="Lambda O")   
CompareSpinOpMat(SpinLambdaR,OpLambdaR,BasisList;title="Lambda R")   

SpinPhysProj = ((I4 + LambdaL*SpinLambdaL)
                *(I4 + LambdaO*SpinLambdaO)
                *(I4 + LambdaR*SpinLambdaR))/(2^3)
OpPhysProj = ((1 + LambdaL*OpLambdaL)
                *(1 + LambdaO*OpLambdaO)
                *(1 + LambdaR*OpLambdaR))*(1//(2^3))

@test SpinPhysProj == SpinPhysProj * SpinPhysProj
@test OpPhysProj == OpPhysProj * OpPhysProj

CompareSpinOpMat(SpinPhysProj,OpPhysProj,BasisList;title="Physical Proj")   


###TotalSpin
MTotZ = sum(diag(MSz(n)) for n in 1:NumSites)
SpinParity = mod.(div.(MTotZ .+ 6,2),2)
ProjDiag = real(diag(SpinPhysProj)*4)
###Note that the projection weight is the same as the spin parity
###We thus see that in the physical space then the spins come in pairs...
###He we work out which states to keep
Lambda=LambdaL*LambdaO*LambdaR
if Lambda == -1
    ###Only odd parity states
    @test isapprox(ProjDiag,SpinParity)
elseif Lambda == 1
    ###Only even parity states
    @test all(isapprox.(ProjDiag+SpinParity,1))
else
    error("Now valid Lambda=$Lambda")
end

PhysHsize = 2^(NumSites - NumPlaques)

PSpinHam = SpinPhysProj* Hspin * SpinPhysProj
POpHam = OpPhysProj* Hmaj * OpPhysProj
CompareSpinOpMat(PSpinHam,POpHam,BasisList;title="Projected Ham")   


###The eigenvalues before before projection 
PSpinEvals = GetEvals(PSpinHam,PhysHsize)


BasisListLambda = BasisList[ProjDiag .== true]

HspinLambda = Hspin[ProjDiag .== true,ProjDiag .== true] ###THis is the wrong projector
PHspinLambda = PSpinHam[ProjDiag .== true,ProjDiag .== true]
SpinPhysProjLambda = SpinPhysProj[ProjDiag .== true,ProjDiag .== true]



HSpinLambdaEvals = GetEvals(HspinLambda,PhysHsize)
PHspinLambdaEvals = GetEvals(PHspinLambda,PhysHsize)

@test  isapprox(PSpinEvals,PHspinLambdaEvals)


####Now that we have thrown away the states lets reorganize the projectors a bit
### Know that there are four states, so let's try and construct these..
###Thus we expect at each level to get 8 / 2 = 4 states


####Now we know which energies to aim for...
###How we now find this in our reduced basis
Perm1=sortperm(-abs.(SpinPhysProjLambda[1,:]))
Perm1PHspinLambda = PHspinLambda[Perm1,Perm1]
Perm1SpinPhysProjLambda = SpinPhysProjLambda[Perm1,Perm1]
Perm1BasisListLambda = BasisListLambda[Perm1]
###We only need to do this one


####Looking at this carefully we see that that is happening is that we can map spins along the z-bonds such that  4~2 , 1~3. The precise sign then depends on the particular realization of the bond-variables...
###Also the hamiltonian is "blocked" in the sense that abs(PHP[i,j]) is constant if for all i comming from the same equvalence class of states.


####Let us now construct the hamiltonian pertain to the c majoranas

CCL = [ c(1), c(4)].*Ket()
CCR = [ c(2), c(3)].*Ket()
CCBasis = [ CCL ; CCR ]
NumHCC =  MakeNumHam(HCC,CCBasis)
NumACC =  NumHCC[1:2,3:4]
###The hamiltonian here is Bipartite in the sense that
## HCC  = i *CCL^T * A * CCR = - i * CCR^T * A * CCL
###First we work out the singular values (these will be wrong)
svdAmat = svd(NumACC).S

###we note that that the largest eigenvalue is the sum of the singular values
@test ( isapprox(+svdAmat[1]+svdAmat[2],PSpinEvals[2])
        || isapprox(+svdAmat[1]-svdAmat[2],PSpinEvals[2]))

