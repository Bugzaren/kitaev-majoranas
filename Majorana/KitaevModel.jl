include("MajoranaReps.jl")

include("TestMajoranas.jl")


using PyPlot
###Here we implement the 3-site spin model with couplinga A,B,C.x

###We have 2^3  states given 

ParityOp1=ParityOp(1)
ParityOp2=ParityOp(2)
ParityOp3=ParityOp(3)

####Check some properties of the parity operator
print("ParityOp1: ")
OpPrintln(ParityOp1)
print("ParityOp1^2: ")
OpPrintln(ParityOp1*ParityOp1)
print("ParityOp1 |0> = ")
OpPrintln(ParityOp1*Down())
print("ParityOp1 c_1|0> = ")
OpPrintln(ParityOp1*c(1)*Down())

###Check parity commutes with the spin operators
OpComutes(ParityOp(1),Sx(1))
OpComutes(ParityOp(1),Sy(1))
OpComutes(ParityOp(1),Sz(1))

###Define the spin states
SpinStates=Dict()
SpinStates[(0,0,0)]=Down()
SpinStates[(1,0,0)]=Up(1)
SpinStates[(0,1,0)]=Up(2)
SpinStates[(0,0,1)]=Up(3)
SpinStates[(1,1,0)]=Up(1,2)
SpinStates[(1,0,1)]=Up(1,3)
SpinStates[(0,1,1)]=Up(2,3)
SpinStates[(1,1,1)]=Up(1,2,3)
SpinLabels=[(0,0,0),(1,0,0),(0,1,0),(0,0,1),
            (1,1,0),(1,0,1),(0,1,1),(1,1,1)]

### Bond operators
up12=im*by(1)*bz(2)
u12=im*bx(1)*bx(2)
u23=im*by(2)*by(3)
u31=im*bz(3)*bz(1)
FluxFactor1=u12*u23*u31
FluxFactor2=u12*up12
print("FluxFactor1: ")
OpPrintln(FluxFactor1)
print("FluxFactor1^2: ")
OpPrintln(FluxFactor1*FluxFactor1)
print("FluxFactor2: ")
OpPrintln(FluxFactor2)
print("FluxFactor2^2: ")
OpPrintln(FluxFactor2*FluxFactor2)


###Check that the operators commute
OpComutes(up12,u12)
OpComutes(up12,u23)
OpComutes(up12,u31)
OpComutes(u12,u23)
OpComutes(u12,u31)
OpComutes(u23,u31)
##Check that the bond operators commute with the projectors
OpNonComutes(ParityOp1,u12)
OpNonComutes(ParityOp1,u31)
OpComutes(ParityOp1,FluxFactor1)
OpComutes(ParityOp2,FluxFactor1)
OpComutes(ParityOp3,FluxFactor1)
OpComutes(ParityOp1,FluxFactor2)
OpComutes(ParityOp2,FluxFactor2)
OpComutes(ParityOp3,FluxFactor2)

###This shows that the bond variables by themselves do not commute with the site projectors
###But the flux variables do
###Now the fun stuff begins - we act with the flux opertors to ceate eigenstates
PList=fill(c(2)+c(1),4)
PList[1]=(1+FluxFactor1)*(1+FluxFactor2)
PList[2]=(1-FluxFactor1)*(1+FluxFactor2)
PList[3]=(1+FluxFactor1)*(1-FluxFactor2)
PList[4]=(1-FluxFactor1)*(1-FluxFactor2)


###The pieces of the hamiltonian are:
HX=Sx(1)*Sx(2)
HY=Sy(2)*Sy(3)
HZ=Sz(3)*Sz(1)
Hp=Sy(1)*Sz(2)

HXmat=fill(0im,8,8)
HYmat=fill(0im,8,8)
HZmat=fill(0im,8,8)
Hpmat=fill(0im,8,8)

### Now we compute the matrix projections
for i in 1:8
    for j in 1:8
        HXmat[i,j]=OpInnerProd(SpinStates[SpinLabels[i]],
                               HX,SpinStates[SpinLabels[j]])
        HYmat[i,j]=OpInnerProd(SpinStates[SpinLabels[i]],
                               HY,SpinStates[SpinLabels[j]])
        HZmat[i,j]=OpInnerProd(SpinStates[SpinLabels[i]],
                               HZ,SpinStates[SpinLabels[j]])
        Hpmat[i,j]=OpInnerProd(SpinStates[SpinLabels[i]],
                               Hp,SpinStates[SpinLabels[j]])

    end
end

####Plot the various hamiltonians
figure()
subplot(2,2,1)
imshow(abs.(HXmat))
subplot(2,2,2)
imshow(abs.(HYmat))
subplot(2,2,3)
imshow(abs.(HZmat))
subplot(2,2,4)
imshow(abs.(Hpmat))
quit()




###Here are the four state
S1p=OpReduce(PFF1p,SpinStates[(0,0,0)])
S1m=OpReduce(PFF1m,SpinStates[(0,0,0)])
S2p=OpReduce(PFF1p,SpinStates[(1,0,0)])
S2m=OpReduce(PFF1m,SpinStates[(1,0,0)])
S3p=OpReduce(PFF1p,SpinStates[(0,1,0)])
S3m=OpReduce(PFF1m,SpinStates[(0,1,0)])
S4p=OpReduce(PFF1p,SpinStates[(1,1,0)])
S4m=OpReduce(PFF1m,SpinStates[(1,1,0)])


Pop12p=OpAdd(One,FluxFactor2)
Pop12m=OpAdd(One,OpScale(-1,FluxFactor2))

S1pp=OpReduce(Pop12p,S1p)
S2pp=OpReduce(Pop12p,S2p)
S1pm=OpReduce(Pop12p,S1m)
S2pm=OpReduce(Pop12p,S2m)
S1mp=OpReduce(Pop12m,S1p)
S2mp=OpReduce(Pop12m,S2p)
S1mm=OpReduce(Pop12m,S1m)
S2mm=OpReduce(Pop12m,S2m)

println("S1pp:")
OpPrintln(S1pp)
println()
OpPrintln(S1pm)
println()
OpPrintln(S1mp)
println()
OpPrintln(S1mm)
println("S1pp:")
OpPrintln(S2pp)
println()
OpPrintln(S2pm)
println()
OpPrintln(S2mp)
println()
OpPrintln(S2mm)


IpHmat=fill(0.0im,2,2)
HmatA=fill(0.0im,2,2)
HmatB=fill(0.0im,2,2)
HmatC=fill(0.0im,2,2)

IpHmat[1,1]=OpInnerProd(S1pp,S1pp)
IpHmat[2,1]=OpInnerProd(S2pp,S1pp)
IpHmat[1,2]=OpInnerProd(S1pp,S2pp)
IpHmat[2,2]=OpInnerProd(S2pp,S2pp)


HmatA[1,1]=OpInnerProd(S1pp,Sx(1)*Sx(2),S1pp)
HmatA[2,1]=OpInnerProd(S2pp,Sx(1)*Sx(2),S1pp)
HmatA[1,2]=OpInnerProd(S1pp,OpMul(Sx(1),Sx(2)),S2pp)
HmatA[2,2]=OpInnerProd(S2pp,OpMul(Sx(1),Sx(2)),S2pp)

HmatB[1,1]=OpInnerProd(S1pp,OpMul(Sy(3),Sy(2)),S1pp)
HmatB[2,1]=OpInnerProd(S2pp,OpMul(Sy(3),Sy(2)),S1pp)
HmatB[1,2]=OpInnerProd(S1pp,OpMul(Sy(3),Sy(2)),S2pp)
HmatB[2,2]=OpInnerProd(S2pp,OpMul(Sy(3),Sy(2)),S2pp)


HmatC[1,1]=OpInnerProd(S1pp,OpMul(Sz(3),Sz(1)),S1pp)
HmatC[2,1]=OpInnerProd(S2pp,OpMul(Sz(3),Sz(1)),S1pp)
HmatC[1,2]=OpInnerProd(S1pp,OpMul(Sz(3),Sz(1)),S2pp)
HmatC[2,2]=OpInnerProd(S2pp,OpMul(Sz(3),Sz(1)),S2pp)


println("IpHmat:")
display(IpHmat)
println("HmatA:")
display(HmatA)
println("HmatB:")
display(HmatB)
println("HmatC:")
display(HmatC)
