include("src/MajoranaReps.jl")

#include("src/TestMajoranas.jl")


using PyPlot
using Test
using LinearAlgebra
close("all")

function GetEvals(Ham,NumEvs)
    RawEvals = eigvals(Ham)
    PhysEnergies = sortperm(abs.(RawEvals))[end-NumEvs+1:end]
    return sort(RawEvals[PhysEnergies])
end


function MakeNumHam(OpHam,BasisList)
    HSSize = length(BasisList)
    NumericHam = fill(0.0im,HSSize,HSSize)
    for i in 1:HSSize
        for j in 1:HSSize
            NumericHam[i,j]=OpInnerProd(BasisList[j],OpHam,BasisList[i])
        end
    end
    return NumericHam
end

function CompareSpinOpMat(SMat,OpMat,BasisList;title="")
    if title != ""
        println("Comparing $title")
    end
    NOpMat = MakeNumHam(OpMat,BasisList)
    ###Check that the numerical Hamiltonian is equivalent to the 
    
    if !all(isapprox.(NOpMat,SMat,atol=sqrt(eps())))
        figure(figsize=(12,5))
        subplot(1,3,1)
        imshow(abs.(NOpMat))
        subplot(1,3,2)
        imshow(isapprox.(NOpMat,SMat))
        subplot(1,3,3)
        imshow(abs.(SMat))
        suptitle(title)
        error("Error while testing for $title")
    end
end



###Define the spin operators
sx = [[0,1] [1,0]]
sy = [[0,-im] [im,0]]
sz = [[1,0] [0,-1]]
II  = [[1,0] [0,1]]
I6  =  diagm(0=>ones(2^6))

MSx(n) = MSpin("x",n)
MSy(n) = MSpin("y",n)
MSz(n) = MSpin("z",n)
    
function MSpin(st,n)
    if st=="x"
        op=sx
    elseif st=="y"
        op=sy
    elseif st=="z"
        op=sz
    else
        error("unknown spin $st")
    end
    if n==1
        kron(op, II, II, II, II, II);
    elseif n==2
        kron(II, op, II, II, II, II);
    elseif n==3
        kron(II, II, op, II, II, II);
    elseif n==4
        kron(II, II, II,op, II, II);
    elseif n==5
        kron(II, II, II, II,op, II);
    elseif n==6
        kron(II, II, II, II, II,op);
    else
        error("unknown spin place")
    end
end

###This is the basis crated by taking Kroenecker deltas
BasisList = kron([Sx(1),1],[Sx(2),1],[Sx(3),1],
                 [Sx(4),1],[Sx(5),1],[Sx(6),1]).*Ket()


for n in 1:6
    CompareSpinOpMat(MSx(n),Sx(n),BasisList;title="Sx($n)")
    CompareSpinOpMat(MSy(n),Sy(n),BasisList;title="Sy($n)")
    CompareSpinOpMat(MSz(n),Sz(n),BasisList;title="Sz($n)")
end

###Define the many body spin couplings
ss12 = MSx(1)*MSx(2)
ss23 = MSz(2)*MSz(3)
ss23p = MSy(2)*MSx(3)
ss34 = MSy(3)*MSy(4)
ss45 = MSx(4)*MSx(5)
ss56 = MSz(5)*MSz(6)
ss56p = MSy(5)*MSx(6)
ss61 = MSy(6)*MSy(1)
ss14 = MSz(1)*MSz(4)


###Define the corresponding majorana operators
SM12 = Sx(1)*Sx(2)
SM23 = Sz(2)*Sz(3)
SM23p = Sy(2)*Sx(3)
SM34 = Sy(3)*Sy(4)
SM45 = Sx(4)*Sx(5)
SM56 = Sz(5)*Sz(6)
SM56p =Sy(5)*Sx(6)
SM61 = Sy(6)*Sy(1)
SM14 = Sz(1)*Sz(4)


###Here is the hamitlonian
# (H = -J12*ss12 - J23*ss23 - J23p*ss23p - J34*ss34 - J45*ss45 - 
#     J56*ss56 - J56p*ss56p - J14*ss14);


###Here we implement the 6-site spin model with couplings
###                   x -- (4) -- y
###                  /      |      \ 
###                 (5)     |      (3)
###                /   \    |     /   \
###                y   z    z     z   x
###                x   z    z     z   y
###                \   /    |     \   /
###                 (6)     |      (2)
###                  \      |       /    
###                   y -- (1) --x

####Here we assign values to the bond variables
BondValList=[1,1,-1,1,1,1,1,1,1] ###For now we take them all to be positive, but his will change later

####The hamiltonian and it's couplings
SpinSpinList = [ss12,ss23,ss23p,ss34,ss45,ss56,ss56p,ss61,ss14]
SpinMajoList = [SM12,SM23,SM23p,SM34,SM45,SM56,SM56p,SM61,SM14]
SpinCCList = [im*c(1)*c(2),im*c(2)*c(3),im*c(2)*c(3),im*c(3)*c(4),
              im*c(4)*c(5),im*c(5)*c(6),im*c(5)*c(6),im*c(6)*c(1),im*c(1)*c(4)]
CouplingList = [ -randn() for x in SpinSpinList]
Hspin = sum(SpinSpinList.*CouplingList)
Hmaj = sum(SpinMajoList.*CouplingList)
HCC = sum(-SpinCCList.*BondValList.*CouplingList)




NumSites = 6
NumPlaques=4
NumBonds= 9
###We introduce the parity operators
Dlist = [ParityOp(x) for x in 1:NumSites]
println("Dlist:")
display(Dlist)

##Further we introduce the projectors to the Physical space
Plist = [(1//2)*(1 + ParityOp(x)) for x in 1:NumSites]
println("Plist:")
display(Plist)


##The combined projector is 
FullProj = prod(Plist)
println("FullProj:")
display(FullProj)


####We also have the bond operators
println("Create the bond operators")
BondOpList = [im*bx(1)*bx(2),
              im*bz(2)*bz(3),
              im*by(2)*bx(3),
              im*by(3)*by(4),
              im*bx(4)*bx(5),
              im*bz(5)*bz(6),
              im*by(5)*bx(6),
              im*by(6)*by(1),
              im*bz(1)*bz(4)]

println("Check commutators:")
for i in 1:NumBonds
    for j in 1:NumBonds
        @test OpComutes(BondOpList[i],BondOpList[j])
    end
end

NumBonds = length(BondValList)

##We can create a projector for this configuration as
println("Create the bond projectors")
BondProjList =[1//2 + 1//2*BondValList[indx]*BondOpList[indx] for indx in 1:NumBonds]
##The Full Bon Projector Is then
#FullBondProj = prod(BondProjList)
#@test FullProj*FullProj == FullProj



##We can create a projector for this configuration as

Lambda23=BondValList[2]*BondValList[3]
LambdaOL=BondValList[1]*BondValList[2]*BondValList[4]*BondValList[9]
LambdaOR=BondValList[9]*BondValList[5]*BondValList[6]*BondValList[8]
Lambda56=BondValList[6]*BondValList[7]


OpLambda23=BondOpList[2]*BondOpList[3]
OpLambdaOL=BondOpList[1]*BondOpList[2]*BondOpList[4]*BondOpList[9]
OpLambdaOR=BondOpList[9]*BondOpList[5]*BondOpList[6]*BondOpList[8]
OpLambda56=BondOpList[6]*BondOpList[7]


SpinLambda23=MSy(2)*MSx(3)*MSz(2)*MSz(3)
SpinLambdaOL=-MSx(1)*MSx(2)*MSz(2)*MSz(3)*MSy(3)*MSy(4)*MSz(4)*MSz(1)
SpinLambdaOR=MSx(4)*MSx(5)*MSz(5)*MSz(6)*MSy(6)*MSy(1)*MSz(1)*MSz(4)
SpinLambda56=MSy(5)*MSx(6)*MSz(5)*MSz(6)



CompareSpinOpMat(Hspin,Hmaj,BasisList;title="Unporjected Matrix")   

CompareSpinOpMat(SpinLambda23,OpLambda23,BasisList;title="Lambda 23")   
CompareSpinOpMat(SpinLambda56,OpLambda56,BasisList;title="Lambda 56")   
CompareSpinOpMat(SpinLambdaOL,OpLambdaOL,BasisList;title="Lambda OL")   
CompareSpinOpMat(SpinLambdaOR,OpLambdaOR,BasisList;title="Lambda OR")   

SpinPhysProj = ((I6 + Lambda23*SpinLambda23)
                *(I6 + Lambda56*SpinLambda56)
                *(I6 + LambdaOR*SpinLambdaOR)
                *(I6 + LambdaOL*SpinLambdaOL))/(2^4)
OpPhysProj = ((1 + Lambda23*OpLambda23)
                *(1 + Lambda56*OpLambda56)
                *(1 + LambdaOR*OpLambdaOR)
                *(1 + LambdaOL*OpLambdaOL))*(1//(2^4))

@test SpinPhysProj == SpinPhysProj * SpinPhysProj
@test OpPhysProj == OpPhysProj * OpPhysProj

CompareSpinOpMat(SpinPhysProj,OpPhysProj,BasisList;title="Physical Proj")   


###TotalSpin
MTotZ = sum(diag(MSz(n)) for n in 1:NumSites)
SpinParity = mod.(div.(MTotZ .+ 6,2),2)
ProjDiag = real(diag(SpinPhysProj)*8)
###Note that the projection weight is the same as the spin parity
###We thus see that in the physical space then the spins come in pairs...
###He we work out which states to keep
Lambda=Lambda23*Lambda56*LambdaOL*LambdaOR
if Lambda == 1
    ###Only odd parity states
    @test isapprox(ProjDiag,SpinParity)
elseif Lambda == -1
    ###Only even parity states
    @test all(isapprox.(ProjDiag+SpinParity,1))
else
    error("Now valid Lambda=$Lambda")
end

PhysHsize = 2^(NumSites - NumPlaques)

PSpinHam = SpinPhysProj* Hspin * SpinPhysProj
POpHam = OpPhysProj* Hmaj * OpPhysProj
#CompareSpinOpMat(PSpinHam,POpHam,BasisList;title="Projected Ham")   


###The eigenvalues before before projection 
PSpinEvals = GetEvals(PSpinHam,PhysHsize)


BasisListLambda = BasisList[ProjDiag .== true]

HspinLambda = Hspin[ProjDiag .== true,ProjDiag .== true] ###THis is the wrong projector
PHspinLambda = PSpinHam[ProjDiag .== true,ProjDiag .== true]
SpinPhysProjLambda = SpinPhysProj[ProjDiag .== true,ProjDiag .== true]



HSpinLambdaEvals = GetEvals(HspinLambda,PhysHsize)
PHspinLambdaEvals = GetEvals(PHspinLambda,PhysHsize)

@test  isapprox(PSpinEvals,PHspinLambdaEvals)


####Now that we have thrown away the states lets reorganize the projectors a bit
### Know that there are four states, so let's try and construct these..
###Thus we expect at each level to get 32 / 4 = 8 states


####Now we know which energies to aim for...
###How we now find this in our reduced basis
Perm1=sortperm(-abs.(SpinPhysProjLambda[1,:]))
Perm1PHspinLambda = PHspinLambda[Perm1,Perm1]
Perm1SpinPhysProjLambda = SpinPhysProjLambda[Perm1,Perm1]
Perm1BasisListLambda = BasisListLambda[Perm1]
####Now we do it on the next set
Perm2=sortperm(-abs.(Perm1SpinPhysProjLambda[9,9:end]))
Perm2=[1:8 ; Perm2 .+ 8]
Perm2PHspinLambda = Perm1PHspinLambda[Perm2,Perm2]
Perm2SpinPhysProjLambda = Perm1SpinPhysProjLambda[Perm2,Perm2]
Perm2BasisListLambda = Perm1BasisListLambda[Perm2]
####And again on the next set 
Perm3=sortperm(-abs.(Perm2SpinPhysProjLambda[17,17:end]))
Perm3=[1:16 ; Perm3 .+ 16]
Perm3PHspinLambda = Perm2PHspinLambda[Perm3,Perm3]
Perm3SpinPhysProjLambda = Perm2SpinPhysProjLambda[Perm3,Perm3]
Perm3BasisListLambda = Perm2BasisListLambda[Perm3]


####Looking at this carefully we see that that is happening is that we can map spins along the z-bonds such that  3~2 , 1~4, 5~6. The precise sign then depends on the particular realization of the bond-variables...
###Also the hamiltonian is "blocked" in the sense that abs(PHP[i,j]) is constant if for all i comming from the same equvalence class of states.


####Let us now construct the hamiltonian pertain to the c majoranas

CCL = [ c(1), c(3),c(5)].*Ket()
CCR = [ c(2), c(4),c(6)].*Ket()
CCBasis = [ CCL ; CCR ]
NumHCC =  MakeNumHam(HCC,CCBasis)
NumACC =  NumHCC[1:3,4:6]
###The hamiltonian here is Bipartite in the sense that
## HCC  = i *CCL^T * A * CCR = - i * CCR^T * A * CCL
###First we work out the singular values (these will be wrong)
svdAmat = svd(NumACC).S

###we note that that the largest eigenvalue is the sum of the singular values
if isapprox(sum(svdAmat),-PSpinEvals[1])
    println("Add negative")
    @test isapprox(+svdAmat[1]+svdAmat[3]+svdAmat[2],-PSpinEvals[1])
    @test isapprox(+svdAmat[1]-svdAmat[3]-svdAmat[2],-PSpinEvals[2])
    @test isapprox(-svdAmat[1]+svdAmat[3]-svdAmat[2],-PSpinEvals[4])
    @test isapprox(-svdAmat[1]-svdAmat[3]+svdAmat[2],-PSpinEvals[3])
elseif isapprox(sum(svdAmat),PSpinEvals[4])
    println("Add positive")
    @test isapprox(+svdAmat[1]+svdAmat[3]+svdAmat[2],PSpinEvals[4])
    @test isapprox(+svdAmat[1]-svdAmat[2]-svdAmat[3],PSpinEvals[3])
    @test isapprox(-svdAmat[1]+svdAmat[2]-svdAmat[3],PSpinEvals[2])
    @test isapprox(-svdAmat[1]-svdAmat[2]+svdAmat[3],PSpinEvals[1])
else
    error("???")
end

