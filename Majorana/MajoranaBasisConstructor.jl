sum(LOAD_PATH.==pwd())==0 && push!(LOAD_PATH, pwd()) ###Push the current directory into the path
using ProgressTimer


function getFullMajState()
    ####Now we build that basis that this acts on
    ###We start from the Down() state
    State=fill(Down(),1)
    NewNumStates=1
    NewState=0
    ###The we act with the hmiltonin on all the stsates to see what new appear
    for HamNo in 1:3*NumPairs
        println("\n..........")
        println("Ham No[$HamNo]: ",LocalMajoranaHam[HamNo])
        NumStates=NewNumStates
        for StNo in 1:NumStates
            println("StNo[$StNo]: ",State[StNo])
            NewState=LocalMajoranaHam[HamNo]*State[StNo]
            println("NewState: ",NewState)
            ###Now we loop though all the previous states to see if this state is new
            IsDuplicate=false
            for StNo2 in 1:NewNumStates
                Overlap=OpInnerProd(State[StNo2],NewState)
                #println("Overlap: <NewState|StNo2> = $Overlap")
                if abs(Overlap)!=0
                    IsDuplicate=true
                    println("Already seen...")
                    break
                end
            end
            if !IsDuplicate ###Add the state if if
                println("Push the new state...")
                NewNumStates+=1
                push!(State,NewState)
            end
        end
    end
    return State,NewNumStates
end


function ComputeMatrixElements(HamSet,States)
    NumStates=length(States)
    NumHams=length(HamSet)
    HamMatrix=fill(fill(0.0im,NumStates,NumStates),NumHams)
    TIMESTRUCT=TimingInit()
    BasisNorm=fill(0.0,NumStates)
    for i in 1:NumStates
        BasisNorm[i]=1.0/sqrt(OpInnerProd(States[i],States[i]))
    end
    
    Tindx=1
    for HamNo in 1:NumHams
        LocalHam=fill(0.0im,NumStates,NumStates)
        for i in 1:NumStates
            for j in 1:NumStates
                LocalHam[i,j]=OpInnerProd(States[i],HamSet[HamNo],
                                          States[j])*BasisNorm[i]*BasisNorm[j]
            end
        end
        HamMatrix[HamNo]=copy(LocalHam)
        TIMESTRUCT=TimingProgress(TIMESTRUCT,Tindx,NumHams;Message="Computing Hamiltonian")
    end
    return HamMatrix
end

function PlotHam(HamMats,ABS=false)
    figure()
    NumHams=length(HamMats)
    minmax=maximum([maximum(abs.(H)) for H in HamMats])
    for HamNo in 1:NumHams
        subplot(NumHams/3,3,HamNo)
        if ABS
            imshow(abs.(HamMats[HamNo]),vmin=-minmax,vmax=minmax,cmap="bwr")
        else
            imshow(HamMats[HamNo],vmin=-minmax,vmax=minmax,cmap="bwr")
        end
    end
    suptitle("The original Hamiltonians")
    TotHam=sum(HamMats)
    
    figure()
    vminmax=maximum(abs.(TotHam))
    if ABS
        imshow(abs.(TotHam),vmin=-vminmax,vmax=vminmax,cmap="bwr")
    else
        imshow(TotHam,vmin=-vminmax,vmax=vminmax,cmap="bwr")
    end
    colorbar()
    suptitle("The Total original Hamiltonian")
end
