using MathLink

println("Loading MathLinkHeader.jl")

import Base.+
import Base.*
import Base.-
import Base./
import Base.//
import Base.^
#### + ####
+(a::MathLink.WExpr,b::MathLink.WExpr)=weval(W"Plus"(a,b))
+(a::MathLink.WExpr,b::MathLink.WSymbol)=weval(W"Plus"(a,b))
+(a::MathLink.WExpr,b::Number)=weval(W"Plus"(a,b))
+(a::MathLink.WSymbol,b::MathLink.WExpr)=weval(W"Plus"(a,b))
+(a::MathLink.WSymbol,b::MathLink.WSymbol)=weval(W"Plus"(a,b))
+(a::MathLink.WSymbol,b::Number)=weval(W"Plus"(a,b))
+(a::Number,b::MathLink.WExpr)=weval(W"Plus"(a,b))
+(a::Number,b::MathLink.WSymbol)=weval(W"Plus"(a,b))
#### - ####
-(a::MathLink.WSymbol)=weval(W"Minus"(a))
-(a::MathLink.WExpr)=weval(W"Minus"(a))
-(a::MathLink.WExpr,b::MathLink.WExpr)=weval(W"Plus"(a,W"Minus"(b)))
-(a::MathLink.WExpr,b::MathLink.WSymbol)=weval(W"Plus"(a,W"Minus"(b)))
-(a::MathLink.WExpr,b::Number)=weval(W"Plus"(a,W"Minus"(b)))
-(a::MathLink.WSymbol,b::MathLink.WExpr)=weval(W"Plus"(a,W"Minus"(b)))
-(a::MathLink.WSymbol,b::MathLink.WSymbol)=weval(W"Plus"(a,W"Minus"(b)))
-(a::MathLink.WSymbol,b::Number)=weval(W"Plus"(a,W"Minus"(b)))
-(a::Number,b::MathLink.WExpr)=weval(W"Plus"(a,W"Minus"(b)))
-(a::Number,b::MathLink.WSymbol)=weval(W"Plus"(a,W"Minus"(b)))

#### * ####
*(a::MathLink.WExpr,b::MathLink.WExpr)=weval(W"Times"(a,b))
*(a::MathLink.WExpr,b::MathLink.WSymbol)=weval(W"Times"(a,b))
*(a::MathLink.WExpr,b::Number)=weval(W"Times"(a,b))
*(a::MathLink.WExpr,b::Rational)=weval(W"Times"(a,b.num, W"Power"(b.den, -1)))
*(a::MathLink.WSymbol,b::MathLink.WExpr)=weval(W"Times"(a,b))
*(a::MathLink.WSymbol,b::MathLink.WSymbol)=weval(W"Times"(a,b))
*(a::MathLink.WSymbol,b::Number)=weval(W"Times"(a,b))
*(a::MathLink.WSymbol,b::Rational)=weval(W"Times"(a,b.num, W"Power"(b.den, -1)))
*(a::Number,b::MathLink.WExpr)=weval(W"Times"(a,b))
*(a::Number,b::MathLink.WSymbol)=weval(W"Times"(a,b))
#### / ####
/(a::MathLink.WExpr,b::MathLink.WExpr)=weval(W"Times"(a, W"Power"(b, -1)))
/(a::MathLink.WExpr,b::MathLink.WSymbol)=weval(W"Times"(a, W"Power"(b, -1)))
/(a::MathLink.WExpr,b::Number)=weval(W"Times"(a, W"Power"(b, -1)))
/(a::MathLink.WExpr,b::Rational)=weval(W"Times"(a*b.den, W"Power"(b.num, -1)))
/(a::MathLink.WSymbol,b::MathLink.WExpr)=weval(W"Times"(a, W"Power"(b, -1)))
/(a::MathLink.WSymbol,b::MathLink.WSymbol)=weval(W"Times"(a, W"Power"(b, -1)))
/(a::MathLink.WSymbol,b::Number)=weval(W"Times"(a, W"Power"(b, -1)))
/(a::MathLink.WSymbol,b::Rational)=weval(W"Times"(a*b.den, W"Power"(b.num, -1)))
/(a::Number,b::MathLink.WExpr)=weval(W"Times"(a, W"Power"(b, -1)))
/(a::Number,b::MathLink.WSymbol)=weval(W"Times"(a, W"Power"(b, -1)))



#### / ####
//(a::MathLink.WExpr,b::MathLink.WExpr)=weval(W"Times"(a, W"Power"(b, -1)))
//(a::MathLink.WExpr,b::MathLink.WSymbol)=weval(W"Times"(a, W"Power"(b, -1)))
//(a::MathLink.WExpr,b::Number)=weval(W"Times"(a, W"Power"(b, -1)))
//(a::MathLink.WExpr,b::Rational)=weval(W"Times"(a*b.den, W"Power"(b.num, -1)))
//(a::MathLink.WSymbol,b::MathLink.WExpr)=weval(W"Times"(a, W"Power"(b, -1)))
//(a::MathLink.WSymbol,b::MathLink.WSymbol)=weval(W"Times"(a, W"Power"(b, -1)))
//(a::MathLink.WSymbol,b::Number)=weval(W"Times"(a, W"Power"(b, -1)))
//(a::MathLink.WSymbol,b::Rational)=weval(W"Times"(a*b.den, W"Power"(b.num, -1)))
//(a::Number,b::MathLink.WExpr)=weval(W"Times"(a, W"Power"(b, -1)))
//(a::Number,b::MathLink.WSymbol)=weval(W"Times"(a, W"Power"(b, -1)))


#### ^ ####
^(a::MathLink.WExpr,b::MathLink.WExpr)=weval(W"Power"(a,b))
^(a::MathLink.WExpr,b::MathLink.WSymbol)=weval(W"Power"(a,b))
^(a::MathLink.WExpr,b::Number)=weval(W"Power"(a,b))
^(a::MathLink.WSymbol,b::MathLink.WExpr)=weval(W"Power"(a,b))
^(a::MathLink.WSymbol,b::MathLink.WSymbol)=weval(W"Power"(a,b))
^(a::MathLink.WSymbol,b::Number)=weval(W"Power"(a,b))
^(a::Number,b::MathLink.WExpr)=weval(W"Power"(a,b))
^(a::Number,b::MathLink.WSymbol)=weval(W"Power"(a,b))

WRational(x::Rational) = weval(W"Times"(x.num, W"Power"(x.den, -1)))

MathLink.put(L::MathLink.Link,R::Rational{Int64}) =  MathLink.put(L,WRational(R))


function Wprint(WExpr)
    weval(W"Print"(WExpr))
    return
end



W2Mstr(x::MathLink.WSymbol) = x.name
function W2Mstr(x::MathLink.WExpr)
    Str=x.head.name*"["
    for j in 1:length(x.args)
        if j>1
            Str*=","
        end
        Str*=W2Mstr(x.args[j])
    end
    Str*="]"
    return Str
end
            
