include("src/MajoranaReps.jl")

#include("src/TestMajoranas.jl")

using Test
using PyPlot
close("all")


###Here we implement the 4-site spin model with couplings
###                 (1) -- x -- (2)
###                /   \       /   \ 
###                y   z       y   z
###                \   /       \   /
###                 (3) -- x -- (4)


ket=Ket

###We introduce the parity operators
Dlist = [ParityOp(x) for x in 1:4]
println("Dlist:")
display(Dlist)

##Further we introduce the projectors to the Physical space
Plist = [(1//2)*(1 + ParityOp(x)) for x in 1:4]
println("Plist:")
display(Plist)

##The combined projector is 
FullProj =prod(Plist)
println("FullProj:")
display(FullProj)


####We also have the bond operators
BondOpList = [im*bx(1)*bx(2),
              im*bx(3)*bx(4),
              im*by(1)*by(3),
              im*bz(1)*bz(3),###Ok to change
              im*by(2)*by(4),
              im*bz(2)*bz(4)]##Ok to change
####These all form qunatized elements An the lidts of those are
BondValList=[1,1,1,-1,1,1] ###We take them all to be positive
##We can create a projector for this configuration as
BondProjList =[1//2 + 1//2*BondValList[indx]*BondOpList[indx] for indx in 1:length(BondValList)]
##The Full Bon Projector Is then
FullBondProj = prod(BondProjList)

@test FullProj*FullProj == FullProj
Lambda13=BondValList[3]*BondValList[4]
Lambda24=BondValList[5]*BondValList[6]


###We begin by craeting a naive basis.
###The first basis is that of all even combinations of c(1)'s.
BasisListNaive = [ket(),c(1)*c(2)*ket(),c(1)*c(3)*ket(),c(1)*c(4)*ket(),
             c(2)*c(3)*ket(),c(2)*c(4)*ket(),c(3)*c(4)*ket(),
              c(1)*c(2)*c(3)*c(4)*ket()]
###However this is not a good basis since the parity of operators on at leats one site is odd.It's only the |0> state that is a good one.
###We note that we can do better by instead of c(j) we use Sx(j) such that there is an accompanying spin. 
BasisList = [ prod([Sx(1),Sx(2),Sx(3),Sx(4)].^BinToOcc(x,4))*Ket() for x in 0:15 ]
BasisNameList = [ BinToOcc(x,4) for x in 0:15 ]


###IN this basis the local constraints are automatically satisfied since there is an even number of majoranas on each site.

if false
    BasisIP1=OpInnerProd(BasisListNaive,BasisListNaive)
    BasisIP2=OpInnerProd(BasisList,BasisList)
    figure()
    imshow(abs.(BasisIP1))
    colorbar()
    figure()
    imshow(abs.(BasisIP2))
    colorbar()
end

ProjBasisListNaive = [ FullProj*BaseElem for BaseElem in BasisListNaive]
ProjBasisList1 = [ FullProj*BaseElem for BaseElem in BasisList]
@test ProjBasisList1 == BasisList

####We can now apply the bond projectors and see what we get

BO1ProjBaseList = [ BondProjList[1]*BaseElem for BaseElem in BasisList]
BO2ProjBaseList = [ BondProjList[2]*BaseElem for BaseElem in BasisList]
BO3ProjBaseList = [ BondProjList[3]*BaseElem for BaseElem in BasisList]
BO4ProjBaseList = [ BondProjList[4]*BaseElem for BaseElem in BasisList]
BO5ProjBaseList = [ BondProjList[5]*BaseElem for BaseElem in BasisList]
BO6ProjBaseList = [ BondProjList[6]*BaseElem for BaseElem in BasisList]

BOProjBase1List = [ PO*BasisList[1] for PO in BondProjList]
BOProj1IP=OpInnerProd(BOProjBase1List,BOProjBase1List)
FullBOProjBaseList = [ FullBondProj*BaseElem for BaseElem in BasisList]
FullBOProjIP=OpInnerProd(FullBOProjBaseList,FullBOProjBaseList)
if false
    figure()
    imshow(abs.(BOProj1IP),vmin=0)
    colorbar()
   
    figure()
    imshow(abs.(FullBOProjIP))
    colorbar()
end



using LinearAlgebra
Evs=sort(abs.(eigvals(FullBOProjIP)))
###Looking at the eigenvalues we see that there are only two that are nonzero.
###Thus the basis is actually twodimentional

###We now set out to find the elements that are nonzero
###We begin by throwing away all the states that have zero inner products
AllStates = (collect(1:15))
NormedTOF=[FullBOProjIP[x,x] != 0 for x in 1:15]
NormedStates=AllStates[NormedTOF]
###Here We Expect There to be only two linear combinations that are valid
###We also expect them to be orthogonal, since
StateNoA=NormedStates[1]
StateA=FullBOProjBaseList[StateNoA]
NormA=1//OpSqrt(OpInnerProd(StateA,StateA))
NStateA=StateA*NormA
indx = 2
while indx <= length(NormedStates)
    global indx,StateB,StateNoB
    if OpInnerProd(FullBOProjBaseList[NormedStates[indx]],StateA) == 0
        StateNoB=NormedStates[indx]
        StateB = FullBOProjBaseList[StateNoB]

        break
    end
    indx += 1
end
NormB=1//OpSqrt(OpInnerProd(StateB,StateB))
NStateB=StateB*NormB


###Now we check that |A> and |B> are actrually
### |A> =  1/2(S1 - S3)*(1-S2*S4)|0>
### |B> =  1/2(S2 - S4)*(1-S1*S3)|0>

KetA = FullBondProj*(Sx(1) - Lambda13*Sx(3))*(1 - Lambda24*Sx(2)*Sx(4))*Ket()
KetB = FullBondProj*(Sx(2) - Lambda24*Sx(4))*(1 - Lambda13*Sx(1)*Sx(3))*Ket()

NKetA = KetA * (1// OpSqrt(OpInnerProd(KetA,KetA)))
NKetB = KetB * (1// OpSqrt(OpInnerProd(KetB,KetB)))

@test OpInnerProd(NKetA,NKetA) == 1
@test OpInnerProd(NKetB,NKetB) == 1
@test OpInnerProd(NKetA,NKetB) == 0

@test NKetA == NStateA
@test NKetA != NStateB
@test NKetB == NStateB


for indx in 1:6
    println("indx = ",indx)
    @test BondOpList[indx]*NKetA ==  BondValList[indx]*NKetA
    @test BondOpList[indx]*NKetB ==  BondValList[indx]*NKetB
end



