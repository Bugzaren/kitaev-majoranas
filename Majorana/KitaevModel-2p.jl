include("MajoranaReps.jl")

include("TestMajoranas.jl")

using PyPlot
close("all")

###Here we implement the 2-site spin model with couplinga A,B,C

###We have 2^2  states given 

ParityOp1=ParityOp(1)
ParityOp2=ParityOp(2)

###Define the spin states
S00=Down()
S10=Up(1)
S01=Up(2)
S11=Up(1,2)
SList=[S00,S10,S01,S11]

###Then we define the Hamiltonian
HamOpX=Sx(1)*Sx(2)
HamOpY=Sy(1)*Sy(2)
HamOpZ=Sz(1)*Sz(2)
HamX=fill(0.0im,4,4)
HamY=fill(0.0im,4,4)
HamZ=fill(0.0im,4,4)
for i in 1:4
    println("Spint state no $i:", OpToStr(SList[i]))
end

###Then we compute the elements of the hamiltonian
for i in 1:4
    for j in 1:4
        HamX[i,j]=OpInnerProd(SList[i],HamOpX,SList[j])
        HamY[i,j]=OpInnerProd(SList[i],HamOpY,SList[j])
        HamZ[i,j]=OpInnerProd(SList[i],HamOpZ,SList[j])
    end
end
println("HamX:")
display(HamX)
println("HamY:")
display(HamY)
println("HamZ:")
display(HamZ)

if false
    figure()
    subplot(1,3,1)
    imshow(abs.(HamX))
    subplot(1,3,2)
    imshow(abs.(HamY))
    subplot(1,3,3)
    imshow(abs.(HamZ))
end

###...................................................
###                    Symmetry sectors 
###...................................................
###We now apply the symmetry sectors to the hamiltonian

### Bond operators
ux=im*bx(1)*bx(2)
uy=im*by(1)*by(2)
uz=im*bz(1)*bz(2)

FluxFactor1=uz*uy
FluxFactor2=ux*uz
print("FluxFactor1: ")
OpPrintln(FluxFactor1)
print("FluxFactor2: ")
OpPrintln(FluxFactor2)


###Check that the operators commute
OpComutes(ux,uy)
OpComutes(ux,uz)
OpComutes(uz,uy)

##Check that the bond operators commute with the projectors
##(but not the parity operators)
OpNonComutes(ParityOp1,ux)
OpNonComutes(ParityOp1,uy)
OpNonComutes(ParityOp1,uz)
OpComutes(FluxFactor1,FluxFactor2)
OpComutes(ParityOp1,FluxFactor1)
OpComutes(ParityOp2,FluxFactor1)
OpComutes(ParityOp1,FluxFactor2)
OpComutes(ParityOp2,FluxFactor2)


###Define projectors onto the flux factors
Proj1p=1+FluxFactor1
Proj1m=1-FluxFactor1
Proj2p=1+FluxFactor2
Proj2m=1-FluxFactor2
println("Single projectors:")
print("P1p: ");OpPrintln(Proj1p,BreakLines=false)
print("P1m: ");OpPrintln(Proj1m,BreakLines=false)
print("P2p: ");OpPrintln(Proj2p,BreakLines=false)
print("P2m: ");OpPrintln(Proj2m,BreakLines=false)

###And the combined projectors
println("Multiple projectors:")
Proj1p2p=Proj1p*Proj2p
Proj1p2m=Proj1p*Proj2m
Proj1m2p=Proj1m*Proj2p
Proj1m2m=Proj1m*Proj2m
PList=[Proj1p2p,Proj1p2m,Proj1m2p,Proj1m2m]


print("P1p2p:");OpPrintln(Proj1p2p)

### Here are the result of projecting the base states onto the diffent sectors
for i in 1:4
    println(" ------   |$i>:")
    print("P1p2p |$i>: ");OpPrintln(Proj1p2p*SList[i],BreakLines=false)
    print("P1p2m |$i>: ");OpPrintln(Proj1p2m*SList[i],BreakLines=false)
    print("P1m2p |$i>: ");OpPrintln(Proj1m2p*SList[i],BreakLines=false)
    print("P1m2m |$i>: ");OpPrintln(Proj1m2m*SList[i],BreakLines=false)
end


###The four states are now (we select some for projection)
###
Psi1p2p=.5*Proj1p2p*S10
Psi1p2m=.5*Proj1p2m*S00
Psi1m2p=.5*Proj1m2p*S00
Psi1m2m=.5*Proj1m2m*S10
ProjList=[Psi1p2p,Psi1p2m,Psi1m2p,Psi1m2m]
ProjName=["+,+","+,-","-,+","-,-"]


Tex=false
println("Psi1p2p:\n",OpToStr(Psi1p2p,tex=Tex,BreakLines=false))
println("Psi1p2m:\n",OpToStr(Psi1p2m,tex=Tex,BreakLines=false))
println("Psi1m2p:\n",OpToStr(Psi1m2p,tex=Tex,BreakLines=false))
println("Psi1m2m:\n",OpToStr(Psi1m2m,tex=Tex,BreakLines=false))


#### We now apply the hamiltonian on the diffent states
### And we should expect it to be diagonal
###Then we compute the elements of the hamiltonian
HamProjX=fill(0.0im,4,4)
HamProjY=fill(0.0im,4,4)
HamProjZ=fill(0.0im,4,4)
for i in 1:4
    for j in 1:4
        HamProjX[i,j]=OpInnerProd(ProjList[i],HamOpX,ProjList[j])
        HamProjY[i,j]=OpInnerProd(ProjList[i],HamOpY,ProjList[j])
        HamProjZ[i,j]=OpInnerProd(ProjList[i],HamOpZ,ProjList[j])
    end
end
println("HamProjX:")
display(HamProjX)
println("HamProjY:")
display(HamProjY)
println("HamProjZ:")
display(HamProjZ)

if false
    figure()
    subplot(1,3,1)
    imshow(real.(HamProjX),cmap="bwr")
    subplot(1,3,2)
    imshow(real.(HamProjY),cmap="bwr")
    subplot(1,3,3)
    imshow(real.(HamProjZ),cmap="bwr")
end

##### --------------- Projecting on the hamiltonian ------------
### let us now wegde the hamiltonian between the four projectors and see what parts surive

###Initalize with some linear combination so wee get the units correct
HamPX=fill(Sx(1)+Sy(1),4,4)
HamPY=fill(Sx(1)+Sy(1),4,4)
HamPZ=fill(Sx(1)+Sy(1),4,4)
for i in 1:4
    for j in 1:4
        HamPX[i,j]=PList[i]*HamOpX*PList[j]
        HamPY[i,j]=PList[i]*HamOpY*PList[j]
        HamPZ[i,j]=PList[i]*HamOpZ*PList[j]
    end
end

###Here are the shapes of sizes of the prjected hamiltonians 
for i in 1:4
    println("--------------i = $i --------------")
    print("HamPX[$i,$i]: ");OpPrintln((1/4)*HamPX[i,i],tex=true)
    print("HamPY[$i,$i]: ");OpPrintln((1/4)*HamPY[i,i],tex=true)
    print("HamPZ[$i,$i]: ");OpPrintln((1/4)*HamPZ[i,i],tex=true)
    end
end

#### We see that after we invoke that ux and uy are constants we are essnetially left with c1 and c2.


###quit()




