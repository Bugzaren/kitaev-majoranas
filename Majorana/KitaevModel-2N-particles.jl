include("MajoranaReps.jl")

include("TestMajoranas.jl")
include("MajoranaBasisConstructor.jl")



using PyPlot
using LinearAlgebra

close("all")

###Here we implement the 2N-site spin model with couplings Special Copulings

###Fist we fix N
NumPairs=1

PlotRawHam=false
PlotPermutedHam=false
PlotMajoranaHam=false
PlotSingleMajoranaHam=false

Jvals=fill(1.0,3*NumPairs)
Jvals=rand(3*NumPairs)
Jvals=(1:3*NumPairs) // (3 *NumPairs)
Jvals=fill(1,3*NumPairs)


println("Make The FLux and Link variables")
FluxValues=fill(-1,NumPairs+1) ###Fix the sector
#FluxValues[NumPairs+1]=+1 ###Here we make a choise <-----
#FluxValues[1]=+1
FluxValues=rand([-1,1],NumPairs+1)
#FluxValues=[-1,1,1,-1,1,-1]


####
LinkValues=fill(-1,3*NumPairs)
#### We will set the link variables suhc that they recover represent flux values
for i in 1:NumPairs
    LinkValues[3*i-2]=1
    LinkValues[3*i-1]=FluxValues[i]
    LinkValues[3*i-0]=1
end 
LinkValues[3*NumPairs]=FluxValues[NumPairs+1]



println("Construct the parity operators")
ParityProjectors=fill(Sx(1)+Sy(1),2*NumPairs)
TotalPP=1
for i in 1:(2*NumPairs)
    global TotalPP
    ParityProjectors[i]=(1//2)*(1+bx(i)*by(i)*bz(i)*c(i))
    TotalPP*=ParityProjectors[i]
end

####We define the flux sectors
BondOperators=fill(Sx(1)+Sy(1),3*NumPairs)
FluxOperators=fill(Sx(1)+Sy(1),NumPairs+1)
HamTerms=fill(Sx(1)+Sy(1),3*NumPairs)

for i in 1:NumPairs
    BondOperators[3*i-2]=im*bx(2*i-1)*bx(2*i)
    BondOperators[3*i-1]=im*by(2*i-1)*by(2*i)
    BondOperators[3*i-0]=im*bz(2*i)*bz(mod(2*i+1,2*NumPairs))
end

FluxO=1
for i in 1:NumPairs
    global FluxO
    FluxOperators[i]=im*bx(2*i-1)*bx(2*i)*im*by(2*i-1)*by(2*i)
    FluxO=im*bx(2*i-1)*bx(2*i)*im*bz(2*i)*bz(mod(2*i+1,2*NumPairs))*FluxO
    println("FluxO: ",OpToStr(FluxO))
    println("FluxOperators[$i]: ",OpToStr(FluxOperators[i]))
    HamTerms[3*i-2]=Sx(2*i-1)*Sx(2*i)*Jvals[3*i-2]
    HamTerms[3*i-1]=Sy(2*i-1)*Sy(2*i)*Jvals[3*i-1]
    HamTerms[3*i-0]=Sz(2*i)*Sz(mod(2*i+1,2*NumPairs))*Jvals[3*i-0]
end
FluxOperators[NumPairs+1]=FluxO
for i in 1:(3*NumPairs)
    println("HamTerms[$i]: ",OpToStr(HamTerms[i]))
end
    



### We will have a look and think about using the link variables as the projector
###




############# 
println("COnstruct the projector operators")

ProjectorOperators=fill(Sx(1)+Sy(1),NumPairs+1)
for i in 1:(NumPairs+1)
    ProjectorOperators[i]=(1//2)*(1+FluxValues[i]*FluxOperators[i])
    println("FluxOperators[$i]: ",OpToStr(FluxOperators[i]))
    println("FluxValues[$i]: ",OpToStr(FluxValues[i]))
    println("ProjectorOperators[$i]: ",OpToStr(ProjectorOperators[i]))
end

###Now we build the basis (it consists of 2^(N-1)states)
BasisSize=2^(NumPairs-1)
LocalBasis=fill(Up(1)+Up(2),BasisSize)
for i in 1:BasisSize
    StateTemplate=Down()
    for k in 1:(NumPairs-1)
        if mod(div(i-1,2^(k-1)),2)==1
            StateTemplate=Sx(2*k-1)*Sx(2*k)*StateTemplate
        end
    end
    for k in 1:(NumPairs)
        if FluxValues[k]==1
            StateTemplate=Sx(2*k-1)*StateTemplate
        end
    end
    println("StateTemplate[$i]: ",OpToStr(StateTemplate))
    for j in 1:(NumPairs+1)
        StateTemplate=ProjectorOperators[j]*StateTemplate
        println("StateTemplate[$i,$j]: ",OpToStr(StateTemplate))
    end
    LocalBasis[i]=StateTemplate
end
for i in 1:(2^(NumPairs-1))
        println("LocalBasis[$i]: ",OpToStr(LocalBasis[i]))
end

####Now we check that the local basis in orthogonal

BasisOverlap=ComputeMatrixElements(1,LocalBasis)[1]
if !isapprox(BasisOverlap,diagm(0=>ones(length(LocalBasis))))
    throw("The basis is not othrogonal")
end
             

###Fnow we make one for each of the terms in the hamiltonian
HamMatrixTerms=real(ComputeMatrixElements(HamTerms,LocalBasis))
####
TotalProjectedHam=sum(HamMatrixTerms)

###Print all the hamiltonians


if PlotRawHam
    figure()
    for HamNo in 1:3*NumPairs
        subplot(NumPairs,3,HamNo)
        imshow(HamMatrixTerms[HamNo],vmin=-.5,vmax=.5,cmap="bwr")
    end
    suptitle("The original Hamiltonians")
    
    figure()
    vminmax=maximum(abs.(TotalProjectedHam))
    imshow(TotalProjectedHam,vmin=-vminmax,vmax=vminmax,cmap="bwr")
    colorbar()
    suptitle("The Total original Hamiltonian")
end


####We see that there is a special structure here where the hamiltonian is block diaognalial


####Now we create a second basis by taking linear combinations of the other bases

###We begin by making suprepositions of nearby states
TransMatrix=fill(1,BasisSize,BasisSize)
for i in 1:BasisSize
    for j in 1:BasisSize
        Iindx=mod.(div.(i-1,2 .^ (0:NumPairs)),2)
        Jindx=mod.(div.(j-1,2 .^ (0:NumPairs)),2)
        TransMatrix[i,j]=(-1) ^ (sum(Iindx .* Jindx))
    end
end




###Print all the hamiltonians
if PlotPermutedHam
    figure()
    for HamNo in 1:3*NumPairs
        subplot(NumPairs,3,HamNo)
        imshow(TransMatrix*HamMatrixTerms[HamNo]*TransMatrix,vmin=-.5,vmax=.5,cmap="bwr")
    end
    suptitle("The Rotated Hamiltonians")
end

####Not surpirsingly life os more complicated
###The otated hamiltonian is only diagonal on the x and y terms.... not the z term....
###We will thus need to be more clever....
### We o this by prjecting on the values of the bonds..... (this will break site parity.... by that is ok.....)


###Here is the hopping hamiltonian that we should start with,,,
LocalMajoranaHam=fill(Up(1)+Up(2),3*NumPairs)
for i in 1:(3*NumPairs)
    global LocalMajoranaHam
    LocalMajoranaHam[i]=HamTerms[i]*BondOperators[i]*LinkValues[i]
end

FullMajState,FullMajStates = getFullMajState()
####Now we have a basis (this basis is quite large though....)
HamMajoranaMatrixTerms = ComputeMatrixElements(LocalMajoranaHam,FullMajState)

###The total weight of the matrix
TotalMajoranaHam=sum(HamMajoranaMatrixTerms) 

###Print all the hamiltonians
if PlotMajoranaHam
    figure()
    for HamNo in 1:3*NumPairs
        subplot(NumPairs,3,HamNo)
        imshow(HamMajoranaMatrixTerms[HamNo],vmin=-1,vmax=1,cmap="bwr")
    end
    suptitle("The Local Majorana Matrix")

    figure()
    vminmax=maximum(abs.(TotalMajoranaHam))
    imshow(TotalMajoranaHam,vmin=-vminmax,vmax=vminmax,cmap="bwr")
    colorbar()
    suptitle("The Total Local Majorana Matrix")
end


###         Single majoranas
####  .........................
### We now construct a basis of single majorana states
### There is one majorana sitting on each vertex.
##We will parition them with the odd ones first and the even ones after
SMBasis=fill(c(1)*Down(),2*NumPairs)
for j in 1:NumPairs
    global SMBasis
    SMBasis[j]=c(2*j-1)*Down()
    SMBasis[NumPairs+j]=c(2*j)*Down()
end
####Now we compute the hamiltonian between there


SMajoranaHamMat = ComputeMatrixElements(LocalMajoranaHam,SMBasis)
###The total weight of the matrix
TotalSMajoranaHamMat=sum(SMajoranaHamMat) 

###Print all the hamiltonians
if PlotSingleMajoranaHam
    figure()
    for HamNo in 1:3*NumPairs
        subplot(NumPairs,3,HamNo)
        imshow(real.(im.*SMajoranaHamMat[HamNo]),vmin=-1,vmax=1,cmap="bwr")
    end
    suptitle("The Single Majorana Matrix")

    figure()
    vminmax=maximum(abs.(TotalSMajoranaHamMat))
    imshow(real.(im.*TotalSMajoranaHamMat),vmin=-vminmax,vmax=vminmax,cmap="bwr")
    colorbar()
    suptitle("The Total Single Majorana Matrix")
end



###The matrix should be hermitian and specifically is should be off under transposes (since it's putely imagninary)
if !isapprox(transpose(TotalSMajoranaHamMat),-TotalSMajoranaHamMat,atol=sqrt(eps()))
    throw("Matrix is not square")
end
if !isapprox(TotalSMajoranaHamMat[1:NumPairs,1:NumPairs],zeros(NumPairs,NumPairs),atol=sqrt(eps()))
    throw("Upper square is not zero")
end

##We extrac the matrix in the upper corner
MatCorner = imag(TotalSMajoranaHamMat[1:NumPairs,(NumPairs+1):(2*NumPairs)])
if PlotSingleMajoranaHam
    figure()
    vminmax=maximum(abs.(MatCorner))
    imshow(MatCorner,vmin=-vminmax,vmax=vminmax,cmap="bwr")
    colorbar()
end

### Next we need to diagonalize the corner matrix with an svd

TheSVD= svd(MatCorner)

if !isapprox(TheSVD.U * diagm(0=>TheSVD.S) * TheSVD.Vt,MatCorner, atol=sqrt(eps()))
    throw("Could not recinsturct matrix")
end


##### We begin by constructing the eigenvalues

MajEigs=TheSVD.S
FullEigensystem=eigen(TotalProjectedHam)
TotEigs=FullEigensystem.values

#### We now construct all the possible eigenvalues combinations ###

TotalMajEigs=fill(0.0,2^NumPairs)
TotalMajLabel=fill("",2^NumPairs)
for i in 1:(2^NumPairs)
    println("i: ",i)
    TmpEnergy=0
    TmpLabel=""
    for k in 1:NumPairs
        if mod(div(i-1,2^(k-1)),2)==1
            print(" + [$k]")
            TmpEnergy+=MajEigs[k]
            TmpLabel*="+"
        else
            print(" - [$k]")
            TmpEnergy-=MajEigs[k]
            TmpLabel*="-"
        end
    end
    TotalMajEigs[i]=TmpEnergy
    TotalMajLabel[i]=TmpLabel
    println("")
end
println("MajEigs:")
display(MajEigs)

println("TotalMajEigs:")
display(TotalMajEigs)

println("TotEigs:")
display(TotEigs)


figure(figsize=(3,5))
for i in 1:length(TotEigs)
    plot([-1,1],TotEigs[i].*[1,1],"g-")
end

for i in 1:NumPairs
    plot([-1,1],MajEigs[i].*[1,1],"b:")
end
 plot([-1,1],[0,0],"-.k")

for i in 1:(2^NumPairs)
    plot(0,TotalMajEigs[i],"kp")
    text(.5,TotalMajEigs[i],TotalMajLabel[i],verticalalignment="center",
         size=20,color="red",family="monospace")
end
xlim([-.1,1])
title("Flux-values: $FluxValues")

















##### We will determine the matrix by projecting on the local majorana modes 
####--------------------- Here we build the basis... 
println("Make Link prjectors")
LinkProjectors=fill(Sx(1)+Sy(1),3*NumPairs)
FullProjector=1
for i in 1:(3*NumPairs)
    global FullProjector
    LinkProjectors[i]=1//2+(1//2)*LinkValues[i]*BondOperators[i]
    FullProjector*=LinkProjectors[i]
end

####Now we make the states


println("Construct the larger")

###Now we build the basis (it consists of 2^(N-1)states)
BasisSize=2^(NumPairs-1)
LinkBasis=fill(Up(1)+Up(2),BasisSize)
LinkPPBasis=fill(Up(1)+Up(2),BasisSize)
for i in 1:BasisSize
    StateTemplate=Down()
    for k in 1:NumPairs
        if FluxValues[k]==1
            StateTemplate=Sx(2*k-1)StateTemplate
        end
    end
    for k in 1:(NumPairs-1)
        if mod(div(i-1,2^(k-1)),2)==1
            StateTemplate=Sx(2*k-1)*Sx(2*k)*StateTemplate
        end
    end
    LinkBasis[i]=FullProjector*StateTemplate
    LinkPPBasis[i]=TotalPP*LinkBasis[i]
end
#####  The LinkPPBasis should be the same basis as the one gotten by applying the flux sectors projectors....

####Now we apply the hamiltonian to the link basis states



###Here are the end we look at the terms in the hamitonian

HamLinkMatrixes=ComputeMatrixElements(HamTerms,LinkBasis)
HamLinkPPMatrixes=ComputeMatrixElements(HamTerms,LinkPPBasis)
##The Total Energy is:
SumPP=sum(HamLinkPPMatrixes)
###The reduces Ham terms  are
NaiveHamElems=HamTerms .* BondOperators .* LinkValues
### We expect the value of i*c1*c2 = ux*uy*uz
CValue=-LinkValues[1]*LinkValues[2]*LinkValues[3]
NaiveHamElemsRed=ComputeMatrixElements(NaiveHamElems .* CValue .* (im * c(1)*c(2)),[Down()])
if !isapprox(HamLinkPPMatrixes,NaiveHamElemsRed,atol=sqrt(eps()))
    println("HamLinkPPMatrixes:")
    display(HamLinkPPMatrixes)
    println("NaiveHamElemsRed:")
    display(NaiveHamElemsRed)
    throw("The naive calualtion is not tha emas as the exact one")
end
