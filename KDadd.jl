import Base.+     #### Addition ###
import Base.-     #### Addition ###
+(x::KDType,y::KDType) = KDadd(KDcopy(x),KDcopy(y))
+(x::Integer,y::KDType) = KDadd(x,KDcopy(y))
+(y::KDType,x::Integer) = KDadd(x,KDcopy(y))
###Minus
-(x::KDType,y::KDType) = KDadd(KDcopy(x),-y)
-(x::Integer,y::KDType) = KDadd(x,-y)
-(y::KDType,x::Integer) = KDadd(-x,KDcopy(y))
###-----One operators
-(x::KDType) = KDscale(-1,x)

+(x::KDHamType,y::KDHamType) = KDadd(KDcopy(x),KDcopy(y))
+(x::Integer,y::KDHamType) = KDadd(x,KDcopy(y))
+(y::KDHamType,x::Integer) = KDadd(KDcopy(x),y)
###Minus
-(x::KDHamType,y::KDHamType) = KDadd(KDcopy(x),-y)
-(x::Integer,y::KDHamType) = KDadd(x,-y)
-(y::KDHamType,x::Integer) = KDadd(-x,y)
###-----One operators
-(x::KDHamType) = KDscale(-1,KDcopy(x))


function KDadd(number::Integer,kd::KDType)
    if number==0
        return kd
    else
        return KDTerms(number,kd)
    end
end
function KDadd(a::KD,b::KD)
    if a == b
        KDprod([a],2)
    else
        KDTerms(0,[KDprod(a),KDprod(b)])
    end
end
KDadd(kdpb::KDprod,kd::KD)=KDadd(kd,kdpb)
KDadd(kd::KD,kdpb::KDprod)=KDadd(KDprod([kd],1),kdpb)
function KDadd(kdpa::KDprod,kdpb::KDprod)
    if kdpa.scale==0
        return kdpb
    elseif kdpb.scale==0
        return kdpa
    elseif KDPropto!(kdpa,kdpb) ###Proportional
        if kdpa.scale + kdpb.scale == 0
            return 0
        else
            return KDprod(kdpa.KDList,kdpa.scale + kdpb.scale)
        end
    else ###Not proportional
        return KDTerms(0,[kdpa,kdpb])
    end
end

KDadd(bsp::KDSupPos,a::KD) = KDadd(a,bsp)
KDadd(a::KD,bsp::KDSupPos) = KDadd(KDprod(a),bsp)
KDadd(bsp::KDSupPos,aprod::KDprod)=KDadd(aprod,bsp)
function KDadd(aprod::KDprod,bsp::KDSupPos)
    if aprod.scale==0
        return bsp
    else
        return KDTerms(bsp.x,[[aprod];bsp.kdpl])
    end
end
function KDadd(asp::KDSupPos,bsp::KDSupPos)
    return KDTerms(asp.x+bsp.x,[asp.kdpl;bsp.kdpl])
end



KDadd(y ::KDHamType,x::Integer)=KDadd(x,y)
function KDadd(x::Integer,y ::KDHamType)
    if x == 0
        return KDcopy(y)
    elseif KDisZero(y) == 0
        return x
    else
        error("FIXME: $(x)!=0")
    end
end



KDadd(x::KDHam,y ::KDHam)=KDadd(KDHamProd(x),KDHamProd(y))
KDadd(x::KDHam,y ::KDHamProd)=KDadd(KDHamProd(x),y)
KDadd(x::KDHamProd,y ::KDHam)=KDadd(x,KDHamProd(y))
function KDadd(x::KDHamProd,y ::KDHamProd; verbose=false)
    verbose && println("adding KDHamProd nand KDHamProd")
    verbose && println("adding ",KDshow(x)," \nand ",KDshow(y))
    if KDisZero(x)
        return y
    elseif KDisZero(y)
        return x
    else
        if KDpropto(x,y) ##Proportional
            x2 = KDcopy(x)
            x2.ham.scale += y.ham.scale
            if x2.ham.scale == 0
                verbose && println("return zero")
                return 0
            else
                verbose && println("return ",KDshow(x2))
                return x2
            end
        else
            SupPos=KDHamProdSupPos(sort!([KDcopy(x),KDcopy(y)]))
            verbose && println("return ",KDshow(SupPos))
            return SupPos
        end
    end
end



KDadd(y ::KDHam,x::KDHamProdSupPos)=KDadd(x,KDHamProd(KDcopy(y)))
KDadd(x::KDHamProdSupPos,y ::KDHam)=KDadd(x,KDHamProd(KDcopy(y)))

KDadd(y ::KDHamProd,x::KDHamProdSupPos)=KDadd(x,y)
function KDadd(x::KDHamProdSupPos,y ::KDHamProd; verbose=false)
    verbose && println("------------------------")
    verbose && println("adding KDHamProdSupPos and KDHamProd")
    verbose && println("adding ",KDshow(x),"\nand ",KDshow(y))
    ###Add the new terms
    x2=KDcopy(x)
    Broke=false
    for indx in 1:length(x2.kdhpList)
        if KDpropto(x2.kdhpList[indx],y)
            verbose && println("proprtional ",KDshow(x2.kdhpList[indx]))
            newTerm = x2.kdhpList[indx] + y
            if KDisZero(newTerm)
                deleteat!(x2.kdhpList,indx)
            else
                x2.kdhpList[indx] = newTerm
            end
            Broke=true
            break
        end
    end
    if !Broke
        ###Add term
        append!(x2.kdhpList,[KDcopy(y)])
    end
    verbose && println("After added term ",KDshow(x2))
    if length(x2.kdhpList) == 1
        ##If there is only one term
        #return that term#
        return x2.kdhpList[1]
    end
    ###Sort the terms
    sort!(x2.kdhpList)
    ###Merge terms that are the same
    verbose && println("Returning ",KDshow(x2))

    return x2
end


function KDadd(x::KDHamProdSupPos,y ::KDSumHamProd; verbose=false)
    verbose && println("adding KDHamProdSupPos and KDSumHamProd")
    verbose && println("adding ",KDshow(x),"\nand ",KDshow(y))
    ###Upgrate the HamPRodSupPos to a SumHamProdSup
    kdshpList = [ KDSumHamProd(KDcopy(kd.ham),KDcopy(kd.prod),[]) for kd in x.kdhpList ]
    UPX = KDSumHamProdSupPos(kdshpList)
    return UPX + y
end

function KDadd(x::KDHamProdSupPos,y ::KDSumHamProdSupPos; verbose=false)
    verbose && println("adding KDHamProdSupPos and KDHamProdSupPos")
    verbose && println("adding ",KDshow(x),"\nand ",KDshow(y))
    Tot = KDcopy(y)
    verbose && println("Tot[0]: ",KDshow(Tot))
    for indx in 1:length(x.kdhpList)
        ###Add terms one by one
        verbose && println("x.kdhpList[$indx]: ",KDshow(x.kdhpList[indx]))
        Tot = Tot + x.kdhpList[indx]
        verbose && println("Tot[$indx]: ",KDshow(Tot))
    end
    verbose && println("Result: ",KDshow(Tot))
    return Tot
end


function KDadd(x::KDHamProdSupPos,y ::KDHamProdSupPos; verbose=false)
    verbose && println("adding KDHamProdSupPos and KDHamProdSupPos")
    verbose && println("adding ",KDshow(x),"\nand ",KDshow(y))
    Tot = KDcopy(y)
    verbose && println("Tot[0]: ",KDshow(Tot))
    for indx in 1:length(x.kdhpList)
        ###Add terms one by one
        verbose && println("x.kdshpList[$indx]: ",KDshow(x.kdhpList[indx]))
        Tot = Tot + x.kdhpList[indx]
        verbose && println("Tot[$indx]: ",KDshow(Tot))
    end
    verbose && println("Result: ",KDshow(Tot))
    return Tot
end




function KDadd(x::KDSumHamProdSupPos,y ::KDSumHamProdSupPos; verbose=false)
    verbose && println("adding KDSumHamProdSupPos and KDSumHamProdSupPos")
    verbose && println("adding ",KDshow(x),"\nand ",KDshow(y))
    Tot = KDcopy(y)
    verbose && println("Tot[0]: ",KDshow(Tot))
    for indx in 1:length(x.kdshpList)
        ###Add terms one by one
        verbose && println("x.kdshpList[$indx]: ",KDshow(x.kdshpList[indx]))
        Tot = Tot + x.kdshpList[indx]
        verbose && println("Tot[$indx]: ",KDshow(Tot))
    end
    verbose && println("Result: ",KDshow(Tot))
    return Tot
end


KDadd(y ::KDHam,x::KDSumHamProd)=KDadd(x,y)
function KDadd(x::KDSumHamProd,y ::KDHam; verbose=false)
    verbose && println("adding ",KDshow(x)," \nand ",KDshow(y))
    return x + KDHamProd(y,KDprod())
end


function KDadd(x::KDSumHamProd,y ::KDHamProd; verbose=false)
    verbose && println("adding ",KDshow(x)," \nand ",KDshow(y))
    return x + KDSumHamProd(y.ham,y.prod,[])
end


function KDadd(x::KDSumHamProd,y ::KDSumHamProd; verbose=false)
    verbose && println("x: ",x)
    verbose && println("y: ",y)
    verbose && println("adding KDSumHamProd nand KDSumHamProd")
    verbose && println("adding ",KDshow(x)," \nand ",KDshow(y))
    if KDisZero(x)
        verbose && println("y is zero")
        return KDcopy(y)
    elseif KDisZero(y)
        verbose && println("x is zero")
        return KDcopy(x)
    else
        verbose && println("Check proprtionality")
        if KDpropto(x,y) ##Proportional
            verbose && println("x= ",KDshow(x))
            verbose && println("y= ",KDshow(y))
            NewHam=KDHam(x.ham.scale+y.ham.scale,x.ham.IndxList)
            if NewHam.scale == 0
                return 0
            else
                return KDSumHamProd(NewHam,x.prod,x.IndxList)
            end
        else
            return KDSumHamProdSupPos(sort!([KDcopy(x),KDcopy(y)]))
        end
    end
end


function KDadd(y::Integer,x::KDSumHamProdSupPos; verbose=false)
    if y == 0
        return KDcopy(x)
    else
        error("What now?")
    end
end

KDadd(x::KDSumHamProdSupPos,y ::KDHam)=KDadd(y,x)
KDadd(y ::KDHam,x::KDSumHamProdSupPos)=KDadd(x,KDHamProd(y,KDprod()))
KDadd(y ::KDHamProd,x::KDSumHamProdSupPos)=KDadd(x,y)
KDadd(x::KDSumHamProdSupPos,y ::KDHamProd)=KDadd(x,KDSumHamProd(y.ham,y.prod,[]))
KDadd(y ::KDSumHamProd,x::KDSumHamProdSupPos)=KDadd(x,y)
function KDadd(x::KDSumHamProdSupPos,y ::KDSumHamProd; verbose=false)
    verbose && println("  -   -   -   -   -")
    verbose && println("adding KDSumHamProdSupPos and KDSumHamProd")
    verbose && println("adding ",KDshow(x),"\nand ",KDshow(y))
    ###Add the new terms
    x2=KDcopy(x)
    Broke=false
    for indx in 1:length(x2.kdshpList)
        TmpTerm=x2.kdshpList[indx]
        verbose && println("$indx of $(length(x2.kdshpList))\ncomparing ",
                           KDshow(TmpTerm),"\nand ",KDshow(y))
        verbose && println("TmpTerm: ",TmpTerm)
        verbose && println("y: ",y)
        if KDpropto(TmpTerm,y)
            verbose && println("Found proportionality")
            verbose && println("term1 ",KDshow(TmpTerm))
            verbose && println("term2 ",KDshow(y))
            newterm = TmpTerm + y
            if KDisZero(newterm) ###IF they cancel delete
                deleteat!(x2.kdshpList,indx)
            else
                x2.kdshpList[indx] = newterm
            end
            Broke=true
            break
        else
            verbose && println("No proportinality")
        end
    end
    if !Broke
        append!(x2.kdshpList,[KDcopy(y)])
    end
    verbose && println("After added term ",KDshow(x2))
    if length(x2.kdshpList) == 1
        ##If there is only one term
        #return that term#
        return x2.kdshpList[1]
    end
    ###Sort the terms
    sort!(x2.kdshpList)
    ###Merge terms that are the same
    verbose && println("Returning ",KDshow(x2))
    return x2
end

