
#### Equalities
import Base.==     #### Addition ###


==(x::KDHamType,y::KDHamType)=KDEqual(x,y)
==(x::KDprod,y::KDprod) = KDequal(x,y)
==(x::KDSupPos,y::KDSupPos) = KDequal(x,y)



function KDequal(a::KDprod,b::KDprod)
    #println("Checking KDprod equality")
    if a.scale != b.scale ###If scales are differnt they are not the same
        return false
    else ### Check if the KD lists are also the same
        return KDPropto!(a.KDList,b.KDList)
    end
end
function KDequal(a::KDSupPos,b::KDSupPos)
    #println("Checking KDSupPos equality")
    if a.x != b.x ###If scales are differnt they are not the same
        return false
    else ### Check if the KD lists are also the same
        #println("Same constants checking the other terms")
        return KDequal(a.kdpl,b.kdpl)
    end
end
function KDequal(a::Array{KDprod,1},b::Array{KDprod,1})
    ###Sort the products and check if they are the same
    a=KDsort!(a)
    b=KDsort!(b)
    if length(a) != length(b)
        return false
    else ###Check element by element if the are the same
        for indx in 1:length(a)
            if a[indx] != b[indx]
                return false
            end
        end
        ###If no elements where different they must be the same
        return true
    end
end


function KDEqual(x::KDHam,y::KDHam)
    if length(x.IndxList)!=length(y.IndxList)
        return false
    elseif x.scale != y.scale
        return false
    else ##Loop over the indexes
        for indx in 1:length(x.IndxList)
            if x.IndxList[indx] != y.IndxList[indx]
                return false
            end
        end ##If all the elements are the same then all is the same
        return true
    end
end

KDEqual(y::KDHam,x::KDHamProd) = KDEqual(x,KDHamProd(y))
KDEqual(x::KDHamProd,y::KDHam) = KDEqual(x,KDHamProd(y))
function KDEqual(x::KDHamProd,y::KDHamProd,verbose=false)
    verbose && println("   .,.,.,.,.,.  ")
    verbose && println("comparing KDHamProd and KDHamProd")
    verbose && println("comparing x=",KDshow(x)," \nand y=",KDshow(y))
    if x.ham != y.ham
        return false
    else
        return x.prod == y.prod
    end
end

function KDEqual(x::KDSumHamProd,y::KDHam,verbose=false)
    verbose && println("--------------------------")
    verbose && println("comparing KDSumHamProd and KDHam")
    verbose && println("comparing x=",KDshow(x)," \nand y=",KDshow(y))
    ###Check if the sum is zero first
    if length(x.IndxList) != 0
        verbose && println("Indexes present")
        return false
    else ##No indexes, we can thus compare the hamprods
        verbose && println("Check the Hamprods")
        return KDHamProd(x.ham,x.prod) == KDHamProd(y)
    end
end
       


KDEqual(x::KDHam,y::KDHamProdSupPos)=KDEqual(y,KDHamProd(x))
KDEqual(y::KDHamProdSupPos,x::KDHam)=KDEqual(y,KDHamProd(x))

KDEqual(x::KDHamProd,y::KDHamProdSupPos)=KDEqual(y,x)
function KDEqual(x::KDHamProdSupPos,y::KDHamProd)
    if length(x.kdhpList) != 1
        return false
    else
        return x.kdhpList[1] == y
    end
end
function KDEqual(x::KDHamProdSupPos,y::KDHamProdSupPos;verbose=false)
    verbose && println("--------------------------")
    verbose && println("comparing x=",KDshow(x)," \nand y=",KDshow(y))
    if length(x.kdhpList) != length(y.kdhpList)
        return false
    else
        ###Sort the lists
        sort!(x.kdhpList)
        sort!(y.kdhpList)
        for indx in 1:length(x.kdhpList)
            if x.kdhpList[indx] != y.kdhpList[indx]
                return false
            end
        end
        return true ###If all the terms are the same if is the same
    end
end


function KDEqual(x::KDSumHamProdSupPos,y::KDHamProdSupPos; verbose=false)
    ###Upgarde
    y2 = KDSumHamProdSupPos(sort!([KDSumHamProd(KDcopy(tmp)) for tmp in y.kdhpList]))
    return x == y2
end



function KDEqual(x::KDSumHamProdSupPos,y::KDSumHamProdSupPos; verbose=false)
    verbose && println("--------------------------")
    verbose && println("comparing x=",KDshow(x)," \nand y=",KDshow(y))
    if length(x.kdshpList) != length(y.kdshpList)
        return false
    else
        ###Sort the lists
        sort!(x.kdshpList)
        sort!(y.kdshpList)
        for indx in 1:length(x.kdshpList)
            if x.kdshpList[indx] != y.kdshpList[indx]
                return false
            end
        end
        return true ###If all the terms are the same if is the same
    end
end



function KDEqual(x::KDSumHamProd,y::KDSumHamProd;verbose=false)
    if !KDpropto(x,y)
        return false
    else ###If proportional then the scale factors need to agree as well
        return x.ham.scale*x.prod.scale == y.ham.scale*y.prod.scale
    end
end
