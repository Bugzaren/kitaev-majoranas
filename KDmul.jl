
import Base.*     #### Multiplication ###
###Scaling###
*(x::Integer,y::KDType) = KDscale(x,y)
*(y::KDType,x::Integer) = KDscale(x,y)


###Scaling###
*(x::KDHamType,y::KDType) = KDmul(x,y)
*(y::KDType,x::KDHamType) = KDmul(x,y)
*(x::KDHamType,y::Integer) = KDscale(x,y)
*(y::Integer,x::KDHamType) = KDscale(x,y)




function KDscale(scale::Integer,kd::KD)
    if scale==0
        return 0
    elseif scale==1
        return KD(kd.x,kd.y)
    else
        return KDprod([kd],scale)
    end
end
function KDscale(scale::Integer,kdp::KDprod)
    if scale==0
        return 0
    elseif kdp.scale*scale == 1 && length(kdp.KDList)==1
        return kdp.KDList[1]
    else
        return KDprod(kdp.KDList,kdp.scale*scale)
    end
end
function KDscale(scale::Integer,kdsp::KDSupPos)
    if scale==0
        return 0
    else
        KDpList=copy(kdsp.kdpl)
        for indx in 1:length(KDpList)
            KDpList[indx] = KDpList[indx]*scale
        end
        return KDTerms(kdsp.x*scale,KDpList)
    end
end



KDscale(scale::Integer,x::KDHam)=KDscale(x,scale)
function KDscale(x::KDHam,scale::Integer)
    if scale == 1
        return KDcopy(x) ###Do nothing
    elseif scale==0
        return 0
    else
        x2 = KDcopy(x)
        x2.scale = x2.scale * scale
        return x2
    end
end
KDscale(scale::Integer,x::KDHamProd)=KDscale(x,scale)
function KDscale(x::KDHamProd,scale::Integer)
    if scale == 1
        return KDcopy(x) ###Do nothing
    elseif scale==0
        return 0
    else
        x2 = KDcopy(x)
        x2.ham = x2.ham * scale
        return x2
    end
end


KDscale(scale::Integer,x::KDSumHamProd)=KDscale(x,scale)
function KDscale(x::KDSumHamProd,scale::Integer)
    if scale == 1
        return KDcopy(x) ###Do nothing
    elseif scale==0
        return 0
    else
        x2 = KDcopy(x)
        x2.ham = x2.ham * scale
        return x2
    end
end




KDscale(scale::Integer,x::KDHamProdSupPos)=KDscale(x,scale)
function KDscale(x::KDHamProdSupPos,scale::Integer)
    if scale == 1
        return KDcopy(x) ###Do nothing
    elseif scale==0
        return 0
    else
        Tot = 0
        for indx in 1:length(x.kdhpList)
            Tot = Tot + scale*x.kdhpList[indx]
        end
        return Tot
    end
end



KDscale(scale::Integer,x::KDSumHamProdSupPos)=KDscale(x,scale)
function KDscale(x::KDSumHamProdSupPos,scale::Integer)
    if scale == 1
        return KDcopy(x) ###Do nothing
    elseif scale==0
        return 0
    else
        Tot = 0
        for indx in 1:length(x.kdshpList)
            Tot = Tot + scale*x.kdshpList[indx]
        end
        return Tot
    end
end






###Multiplication###
*(x::KDType,y::KDType) = KDmul(x,y)
function KDmul(a::KD,b::KD)
    if a < b
        return KDprod([a,b],1)
    elseif a > b
        return KDprod([b,a],1)
    elseif a == b
        return a ##Product of two equal KD:s is a KD
    else
        error("a=$a and b=$b cannot be ordered")
    end
end
KDmul(b::KD,a::KDprod)=KDmul(a,b)
function KDmul(a::KDprod,b::KD)
    if a.scale==0
        return 0
    else ###Multiply the factor in the correct place
        KDprod(KDreduce!(a.KDList,b),a.scale)
    end
end
function KDmul(a::KDprod,b::KDprod)
    if a.scale==0
        return 0
    elseif b.scale==0
        return 0
    else ###Multiply the factor in the correct place
        KDprod(KDreduce!(a.KDList,b.KDList),a.scale*b.scale)
    end
end

KDmul(b::KDSupPos,a::KD)=KDmul(a,b)
KDmul(a::KD,b::KDSupPos)=KDmul(KDprod(a),b)
KDmul(b::KDSupPos,a::KDprod)=KDmul(a,b)
function KDmul(a::KDprod,b::KDSupPos)
    if a.scale==0
        return 0
    else ##Multiply the factor with all the terms (including the constant)
        NumTerms = length(b.kdpl)
        if b.x == 0
            xList = Array{KDprod,1}(undef,NumTerms)
        else
            xList = Array{KDprod,1}(undef,NumTerms+1)
        end
        #println("xList: ",xList)
        for indx in 1:NumTerms
            xList[indx]=b.kdpl[indx]*a
        end
        if b.x != 0
            #println("a: ",a)
            #println("b.x: ",b.x)
            #println("b.x*a: ",b.x*a)
            xList[NumTerms+1]=b.x*a
        end
        #println("xList: ",xList)
        return KDTerms(0,xList)
    end
end




KDmul(x::KDHam,y::KD)=KDHamProd(x,KDprod(y))
function KDmul(x::KDHam,y::KDprod)
    if x.scale==0
        return 0
    elseif y.scale==0
        return 0
    else
        new_scale=x.scale*y.scale
        y2=KDprod(y.KDList,1)
        return KDHamProd(KDHam(new_scale,KDcopy(x.IndxList)),1*y2)
    end
end
function KDmul(x::KDHamProd,y::KD)
    if x.ham.scale==0
        return 0
    elseif x.prod.scale==0
        return 0
    else
        x2=KDcopy(x)
        x2.prod = x2.prod * y
        return x2
    end
end


KDmul(y::KDSupPos,x::KDHam)=KDmul(x,y)
function KDmul(x::KDHam,y::KDSupPos;verbose=false)
    verbose && println("Ham: ",KDshow(x))
    verbose && println("SupPos: ",KDshow(y))
    Tot=x*y.x
    verbose && print("Tot: ",KDshow(Tot))
    for indx in 1:length(y.kdpl)
        tmp=x*y.kdpl[indx]
        if verbose
            println(" ........")
            println("indx :",indx)
            println("tmp: ",KDshow(tmp))
            println("Tot before: ",KDshow(Tot))
            println("typeof(tmp)=",typeof(tmp))
            println("typeof(Tot)=",typeof(Tot))
        end
            Tot = Tot + tmp
        if verbose
            println("Tot after: ",KDshow(Tot))
        end
    end
    return Tot
end



