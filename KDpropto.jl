
KDpropto(x::KDHam,y::KDHamProd)=KDpropto(KDHamProd(x),y)
KDpropto(y::KDHamProd,x::KDHam)=KDpropto(KDHamProd(x),y)
function KDpropto(x::KDHamProd,y::KDHamProd)
    if KDpropto(x.ham,y.ham)
        if KDpropto(x.prod,y.prod)
            return true
        else
            false
        end
    else
        false
    end
end
KDpropto(x::KDHam,y::KDHam) = (x.IndxList == y.IndxList)
KDpropto(x::KDprod,y::KDprod) = (x.KDList == y.KDList)
KDpropto(x::KD,y::KD) = (x == y)


function KDpropto(x::KDSumHamProd,y::KDSumHamProd;verbose=false)
    ValidateKDSum(x)
    ValidateKDSum(y)
    FreeIndX=KDSumExternalIndexes(x)
    FreeIndY=KDSumExternalIndexes(y)
    verbose && println(" - - - - - - - - -")
    verbose && println("x: ",x)
    verbose && println("y: ",y)
    verbose && println("KDpropto(x::KDSumHamProd,y::KDSumHamProd)")
    verbose && println("Comparing " ,KDshow(x)," \nand ",KDshow(y))
    verbose && println("FreeIndX: " ,KDshow(FreeIndX))
    verbose && println("FreeIndY: " ,KDshow(FreeIndY))
    verbose && println("x.IndxList: ",KDshow(x.IndxList))
    verbose && println("y.IndxList: ",KDshow(y.IndxList))
    if length(x.IndxList) != length(y.IndxList)
        verbose && println("Differnt index lengths")
        return false ##Different nubmer of dummy elemets in sum
    elseif length(FreeIndX) != length(FreeIndY)
        verbose && println("Differnt number of free indexes")
        return false ##Different nubmer of external elemets in sum
    end
    ###Prelare the sorted lists
    SFX=sort!(FreeIndX)
    SFY=sort!(FreeIndY)
    verbose && println("SFX: " ,SFX)
    verbose && println("SFY: " ,SFY)
    if SFX != SFY
        verbose && println("Free indexes do not match")
        ###The Free index lsits have the same length
        ###They also need to be the same to be able to continue
        return false
    else ### We now need to start comparting the summation indexes
        ###Do the simplest, check if all are the same
        SameIndex=sort!(x.IndxList) == sort!(y.IndxList)
        if SameIndex
            verbose && println("All indexes match, make the comparison")
            return KDCompareSumEqualIndex(x,y)
        else
            ###In principle the indexes can be changed, so we need to know which those indexes are
            ###We begin by setting up a translation table
            RenormIndex=FindRenamableIndexes(x.IndxList,y.IndxList)
            verbose && println("RenormIndex :" ,RenormIndex)
            ###Loop though the hamiltoniasn and delta functions and swap the indexes
            for indx in 1:size(RenormIndex)[2]
                y=KDSumSubIndx(y,RenormIndex[1,indx],RenormIndex[2,indx])
                verbose && println("y[$indx]: ",KDshow(y))
            end
            verbose && println("Now all indexes match, make the comparison")
            return KDCompareSumEqualIndex(x,y)
        end
    end
    error("Reached end of if-matrix: Not good")
end




function KDCompareSumEqualIndex(x::KDSumHamProd,y::KDSumHamProd;verbose=false)
    ValidateKDSum(x)
    ValidateKDSum(y)
    verbose && println(" -.,-.,-.,-.,-.,-., ")
    verbose && println("Comparing " ,KDshow(x)," \nand ",KDshow(y))
    if sort!(x.IndxList) != sort!(y.IndxList)
        error("This function assumes the summation indexes are alraedy the same")
    end
    PropHam = KDpropto(x.ham,y.ham)
    SameKDProd = x.prod == y.prod
    verbose && println("PropHam:",PropHam)
    verbose && println("SameKDProd:",SameKDProd)
    if PropHam && SameKDProd
            return true
    elseif !PropHam ###Reorder the indexes of the hamiltonian, if possible
        verbose && println("x.ham: ",KDshow(x.ham))
        verbose && println("y.ham: ",KDshow(y.ham))
        if length(x.ham.IndxList) != length(y.ham.IndxList)
            ###The numbefr of indexes are differnt, can never be same
            return false
        end
        ###Check pair by pair of we can find a pair that is ordered the wrong way
        ###We begin by checking the first pair
        ReorderMap=HamReorderFunction(x.ham,y.ham,x.IndxList)
        verbose && println("ReorderMap:")
        verbose && display(ReorderMap)
        if ReorderMap == [[] []]
            verbose && println("No reordering is possible")
            return false
        end
        y.ham=KDHamPermIndx(y.ham,ReorderMap)
        y.prod=KDProdPermIndx(y.prod,ReorderMap)

        verbose && println("Perm x: ",KDshow(x))
        verbose && println("Perm y: ",KDshow(y))
        ##Call back again
        KDCompareSumEqualIndex(x,y)
    else
        ###Hams he same but not KD prods
        ###For now these are treated as differnt mailtonians
        return false
    end
end

function FindRenamableIndexes(XList_in::Array,YList_in::Array; verbose=false)
    ###We need to find pairs that an remove indexes that are the same
    XList=KDcopy(XList_in)
    YList=KDcopy(YList_in)
    verbose && println("XList :" ,KDshow(XList))
    verbose && println("YList :" ,KDshow(YList))
    if length(XList) != length(YList)
        error("Lists are of unequal length!")
    end
    ##loop of all the indexes and remove if there is a pair
    NumIndexes=length(YList)
    Indx=1
    while Indx <= NumIndexes
        Broken=false
        for I2 in 1:NumIndexes
            if XList[Indx] == YList[I2]
                deleteat!(XList,Indx)
                deleteat!(YList,I2)
                NumIndexes -= 1
                Broken=true
                break
            end
        end
        if !Broken
            Indx += 1
        end
    end
    verbose && println("After Clean XList :" ,KDshow(XList))
    verbose && println("After Clean YList :" ,KDshow(YList))
    IndxPairs=fill(KDs("i"),2,NumIndexes)
    verbose && println("IndxPairs: ",IndxPairs)
    for indx in 1:NumIndexes
        IndxPairs[1,indx] = YList[indx]
        IndxPairs[2,indx] = XList[indx]
    end
    return IndxPairs
end
