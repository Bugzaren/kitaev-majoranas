using Test

include("KD.jl")

@test Kdelta("i","j")*(1+Kdelta("i","j")) == 2*Kdelta("i","j")
@test Kdelta("i","j")*(1-Kdelta("i","j")) == 0
@test MajIP([1,2,"i","j","j","l"]) == -MajIP([2,1,"i","j","j","l"])

@testset "Kdelta algebra" begin
    @test Kdelta(1,0) == 0
    @test Kdelta(1,1) == 1
    @test Kdelta("k","j") == Kdelta("k","j")
    @test Kdelta(1,"j") + 0 == Kdelta(1,"j")
    @test Kdelta("k","j") + 0 == Kdelta("k","j")
    ###More advanced tests
    @test KDprod(KD[KD(1, "j"), KD(2, "i")], 1) == KDprod(KD[KD(1, "j"), KD(2, "i")], 1)
    @test Kdelta(1,"j")*Kdelta(2,"i") == Kdelta(2,"i")*Kdelta(1,"j")
@test Kdelta(2,"j")*Kdelta(2,"j") == Kdelta(2,"j")
@test Kdelta(1,"j")*Kdelta(2,"i")*Kdelta(3,"k") == Kdelta(2,"i")*Kdelta(3,"k")*Kdelta(1,"j")
@test Kdelta(1,"j")*Kdelta(2,"i")*Kdelta(3,"k") == Kdelta(3,"k")*Kdelta(2,"i")*Kdelta(1,"j")
@test KDSupPos(0, KDprod[KDprod(KD[KD(1, "j")], 1), KDprod(KD[KD(2, "i")], 1)]) == KDSupPos(0, KDprod[KDprod(KD[KD(1, "j")], 1), KDprod(KD[KD(2, "i")], 1)])
@test Kdelta(1, "j") + Kdelta(2, "i") == Kdelta(2, "i") + Kdelta(1, "j")

@test Kdelta(1, "j") + Kdelta(2, "i") != Kdelta(2, "i") - Kdelta(1, "j")
@test Kdelta(1, "j")*Kdelta(3,"i") + Kdelta(3,"i")*Kdelta(2, "i") == Kdelta(3,"i")*Kdelta(2, "i") + Kdelta(1, "j")*Kdelta(3,"i")

end


@testset "Majorana IP Reduction 1p" begin
    @test MajIP(1) == 0
    @test MajIP([2]) == 0
    @test MajIP(["k"]) == 0
    @test MajIP("c") == 0
end
@testset "Majorana IP Reduction 2p" begin
    @test MajIP([1,1]) == 1
    @test MajIP([1,2]) == 0
    @test MajIP([2,1]) == 0
    @test MajIP([2,2]) == 1
    @test MajIP(["a","b"]) == Kdelta("b","a")
    @test MajIP([1,"a"]) == Kdelta(1,"a")
    @test MajIP(["a","b"]) == Kdelta("a","b")
end

@testset "Majorana IP Reduction 3p" begin
    @test MajIP([1,2,3]) == 0
    @test MajIP([1,"b",3]) == 0
    @test MajIP([1,2,"r"]) == 0
end
@testset "Majorana IP Reduction 4p" begin
    @test MajIP([1,1,1,1]) == 1
    @test MajIP([1,1,1,2]) == 0
    @test MajIP([1,2,3,4]) == 0
    @test MajIP([1,1,2,2]) == 1
    @test MajIP([1,2,1,2]) == -1
    @test MajIP([1,2,2,1]) == 1
    @test MajIP([1,2,"i",1]) == Kdelta(2,"i")
    @test MajIP([1,1,"i",1]) == MajIP(["i",1])
    @test MajIP([1,1,"i","j"]) == MajIP(["i","j"])
    @test MajIP(["i","i","i","i"]) == 1
    @test MajIP(["i","i","j","j"]) == 1
    @test MajIP(["i","j","j","i"]) == 1
    @test MajIP(["i","j","i","j"]) == 2*Kdelta("i","j") - 1    
@test MajIP([1,2,"i","j"]) == Kdelta(1,"j")*Kdelta(2,"i")-Kdelta(1,"i")*Kdelta(2,"j")
@test MajIP(["i","j","k","l"]) == (Kdelta("i","j")*Kdelta("k","l")
                                   -Kdelta("i","k")*Kdelta("j","l")
                                   +Kdelta("i","l")*Kdelta("k","j"))
end
