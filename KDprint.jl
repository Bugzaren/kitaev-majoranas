#####--------- Adding a print functionnality to make it prettier......


###Print the in delta form natively
import Base.show
show(kd::KD)=KDprint(kd)
#show(kdi::KDi)=print(kdi.x)
#show(kds::KDs)=print(kds.x)


KDprint(kd::KDAllTypes;TeX=false)=print(KDshow(kd,TeX=TeX))
KDprintln(kd::KDAllTypes;TeX=false)=println(KDshow(kd,TeX=TeX))

KDshow(kd::Integer;TeX=false) = "$kd"
KDshow(kd::KDIndex;TeX=false) = "$(kd.x)"


KDshow(List::Array{KDIndex,1};TeX=false) = KDshowIndx(List)
KDshow(List::Array{KDs,1};TeX=false) = KDshowIndx(List)
KDshow(List::Array{KDs,1};TeX=false) = KDshowIndx(List)
function KDshowIndx(List::Array)
    Str="["
    for indx in 1:length(List)
        if indx != 1
            Str*=","
        end
        Str*=KDshow(List[indx])
    end
    Str*="]"
    return Str
end
    

function KDshow(kd::KD;TeX=false)
    if TeX
        return "\\delta_{$(kd.x.x),$(kd.y.x)}"
    else
        return "δ($(kd.x.x),$(kd.y.x))"
    end
end
function KDshow(kd::KDprod;FirstTerm=true,TeX=false)
    Str=KDshowInteger(kd.scale,FirstTerm=FirstTerm,TeX=TeX)
    for indx in 1:length(kd.KDList)
        Str*=KDshow(kd.KDList[indx],TeX=TeX)
    end
    return Str
end
function KDshow(kd::KDSupPos;TeX=false)
    Str=""
    if kd.x!==0
        Str*="$(kd.x)"
        Str*=KDshow(kd.kdpl[1],FirstTerm=false,TeX=TeX)
    else
        Str*=KDshow(kd.kdpl[1],TeX=TeX)
    end
    for indx in 2:length(kd.kdpl)
        Str*=KDshow(kd.kdpl[indx],FirstTerm=false,TeX=TeX)
    end
    return Str
end

function KDshowInteger(Num::Integer;FirstTerm=true,TeX=false)
    if FirstTerm
        if Num==1
            return ""
        elseif Num==-1
            return "-"
        else
            return "$(Num)"
        end
    else
        if Num==1
            return " + "
        elseif Num==-1
            return " - "
        elseif Num>0
            return " + $(Num)"
        else
            return " - $(abs(Num))"
        end
    end        
end


function KDshow(kd::KDHam;FirstTerm=true,TeX=false,PrintScale=true)
    if PrintScale
        Str=KDshowInteger(kd.scale,FirstTerm=FirstTerm,TeX=TeX)
    else
        Str=""
    end
    if TeX
        Str*="h^{($(length(kd.IndxList)))}"
    else
        Str*="h"
    end
    if length(kd.IndxList)!=0
        if TeX
            Str*="_{"
        else
            Str*="("
        end
    end
    for indx in 1:length(kd.IndxList)
        if indx!=1
            Str*=","
        end
        Str*=KDshow(kd.IndxList[indx])
    end
    if length(kd.IndxList)!=0
        if TeX
            Str*="}"
        else
            Str*=")"
        end
    end
    return Str
end
function KDshow(kd::KDHamProd;FirstTerm=true,TeX=false)
    return (KDshow(kd.ham;FirstTerm=FirstTerm,TeX=TeX)*
            KDshow(kd.prod;FirstTerm=true,TeX=TeX))
end
function KDshow(kd::KDSumHamProd;FirstTerm=true,TeX=false)
    Str=KDshowInteger(kd.ham.scale,FirstTerm=FirstTerm,TeX=TeX)
    if length(kd.IndxList)!=0
        if TeX
            Str*="\\sum_{"
        else
            Str*="Σ("
        end
        for indx in 1:length(kd.IndxList)
            if indx!=1
                Str*=","
            end
            Str*=KDshow(kd.IndxList[indx])
        end
        if TeX
            Str*="}"
        else
            Str*=")"
        end
    end
    Str*=KDshow(kd.ham;FirstTerm=FirstTerm,TeX=TeX,PrintScale=false)
    Str*=KDshow(kd.prod;FirstTerm=true,TeX=TeX)
    return Str
end

function KDshow(kd::KDHamProdSupPos;FirstTerm=true,TeX=false)
    Str=""
    for indx in 1:length(kd.kdhpList)
        FirstTerm=(indx==1) ##First erm is first
        Str*=KDshow(kd.kdhpList[indx];FirstTerm=FirstTerm,TeX=TeX)
    end
    return Str
end


function KDshow(kd::KDSumHamProdSupPos;FirstTerm=true,TeX=false)
    Str=""
    for indx in 1:length(kd.kdshpList)
        FirstTerm=(indx==1) ##First erm is first
        Str*=KDshow(kd.kdshpList[indx];FirstTerm=FirstTerm,TeX=TeX)
    end
    return Str
end
