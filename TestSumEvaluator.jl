using Test

include("KD.jl")

@test !KDpropto(KDSumHamProd(KDHam(-1, KDIndex[]), KDprod(KD[KD(KDs("s_1"), KDs("s_3")), KD(KDs("s_2"), KDs("s_4"))], 1), KDIndex[]),
                KDSumHamProd(KDHam(1, KDIndex[]), KDprod(KD[KD(KDs("s_1"), KDs("s_2")), KD(KDs("s_3"), KDs("s_4"))], 1), KDIndex[]))


@test (KDSumHamProd(KDHam(-1, KDIndex[]), KDprod(KD[KD(KDs("s_1"), KDs("s_3")), KD(KDs("s_2"), KDs("s_4"))], 1), KDIndex[]) +
       KDSumHamProd(KDHam(1, KDIndex[]), KDprod(KD[KD(KDs("s_1"), KDs("s_2")), KD(KDs("s_3"), KDs("s_4"))], 1), KDIndex[])
       == KDHam()*(-KD("s_1","s_3")*KD("s_2","s_4") + KD("s_1","s_2")*KD("s_3","s_4")))


@test -KDHam()*KD("s_1","s_3")*KD("s_2","s_4") + KDHam()*KD("s_1","s_2")*KD("s_3","s_4") == KDHam()*(-KD("s_1","s_3")*KD("s_2","s_4") + KD("s_1","s_2")*KD("s_3","s_4"))


@test !KDCompareSumEqualIndex(KDSumHamProd(KDHam(1, KDIndex[KDs("s_1"), KDs("s_2")]), KDprod(KD[], 1), KDIndex[]),
         KDSumHamProd(KDHam(1, KDIndex[]), KDprod(KD[KD(KDs("s_1"), KDs("s_2"))], 1), KDIndex[]),verbose=false)


@test !KDpropto(KDSumHamProd(KDHam(1, KDIndex[KDs("s_1"), KDs("s_2")]), KDprod(KD[], 1), KDIndex[]),
         KDSumHamProd(KDHam(1, KDIndex[]), KDprod(KD[KD(KDs("s_1"), KDs("s_2"))], 1), KDIndex[]),verbose=false)


@test  !KDpropto(KDHam("a","b"),KDHam()*KD("a","b"))
@test  !KDpropto(KDHamProd(KDHam("a","b")),KDHam()*KD("a","b"))



@testset "test printing" begin
@test KDshow(MajIP(["i",1,2,"j"])) == "δ(1,i)δ(2,j) - δ(1,j)δ(2,i)"
@test KDshow(KDHam(1,"t")) == "h(1,t)"
@test KDshow(KDHam("s_1","t"),TeX=true) == "h^{(2)}_{s_1,t}"

@test KDshow(KDHam(1,"t")*KD("l",2)) == "h(1,t)δ(2,l)"
@test KDshow(KDSum(KDHam(1,"t"),"t")) == "Σ(t)h(1,t)"
@test KDshow(KDSum(KDHam(1,"t")*KD("l",2),"t")) == "Σ(t)h(1,t)δ(2,l)"
end


@testset "test a reordering problem" begin

T1 = 2*KDSum(KDHam(1,"l",2,"l"),["l"]) - KDSum(KDHam(1,"l",2,"l"),["l"])
T2 = -KDSum(KDHam(1,"l",2,"l"),["l"])
T3 = 2*KDSum(KDHam(1,"l",2,"l"),["l"]) - 2*KDSum(KDHam(1,"l",2,"l"),["l"])
@test T1 + T2 == T3
@test T1 + T2 == T3
@test T1 + T2 == T3

@test KDpropto(KDSumHamProd(KDHam(-4, KDIndex[KDi(1), KDs("l"), KDi(2), KDs("l")]), KDprod(KD[], 1), KDIndex[KDs("l")]),
               KDSumHamProd(KDHam(4, KDIndex[KDi(1), KDs("l"), KDi(2), KDs("l")]), KDprod(KD[], 1), KDIndex[KDs("l")]),verbose=true)


@test MajFun(0,0,4) == (KD("i","j")*KD("k","l")
                        + KD("i","k")*KD("j","l")
                        - KD("i","l")*KD("j","k"))

@test MajProd(0,0,4) == (KDHam("i","j","k","l")*KD("i","j")*KD("k","l")
                        + KDHam("i","j","k","l")*KD("i","k")*KD("j","l")
                         - KDHam("i","j","k","l")*KD("i","l")*KD("j","k"))


@test SymReduce(MajSum(0,2,4)) - ( 4*KDSum(KDHam(1,"l",2,"l"),["l"])
                                   - 4*KDSum(KDHam(2,"l",1,"l"),["l"])) == 0



@test SymReduce(MajSum(0,2,4)) == ( 4*KDSum(KDHam(1,"l",2,"l"),["l"])
                                   - 4*KDSum(KDHam(2,"l",1,"l"),["l"]))
                        


@test MajSum(0,0,4) - ( KDSum(KDHam("k","l","k","l"),["k","l"])
                         - KDSum(KDHam("l","k","k","l"),["k","l"])
                               + KDSum(KDHam("l","l","k","k"),["k","l"])) == 0

    
@test_broken MajSum(0,0,4) == ( KDSum(KDHam("k","l","k","l"),["k","l"])
                         - KDSum(KDHam("l","k","k","l"),["k","l"])
                                + KDSum(KDHam("l","l","k","k"),["k","l"]))

end





###Tracing down a problem with linking
@testset "Ham sum sub problem" begin
    Term1 = KDHam()
    Term2 = KDHam() + 2*KDHam(1,1)
    Term3= KDcopy(Term2)
    @test Term3 == Term2
    Term3.kdhpList[2] = 2*KDHam(1,2)
    @test Term3 != Term2
    @test Term1 == KDHam()
    @test Term2 - Term1 == 2*KDHam(1,1)
    @test Term1 == KDHam()
    @test Term2 - Term1 == 2*KDHam(1,1)

    Term1 = KDHam() - KDSum(KDHam("j","j"),["j"])
    Term1C = KDcopy(Term1)
    Term2 = KDHam() - KDSum(KDHam("j","j"),["j"]) +2*KDHam(1,1)
    @test Term1 == Term1C
    @test Term2 - Term1 == 2*KDHam(1,1)
    @test Term1 == Term1C
    @test Term2 - Term1 == 2*KDHam(1,1)
    @test Term1 == Term1C
    
    Term1 = KDHam() - KDSum(KDHam("j","j"),["j"])+2*KDSum(KDHam("k","l","k","l"),["k","l"])
    Term1C = KDcopy(Term1)
    Term2 = KDHam() - KDSum(KDHam("j","j"),["j"])+2*KDSum(KDHam("k","l","k","l"),["k","l"]) +2*KDHam(1,1)
    @test Term1 == Term1C
    @test Term2 - Term1 == 2*KDHam(1,1)
    @test Term1 == Term1C
    @test Term2 - Term1 == 2*KDHam(1,1)
    @test Term1 == Term1C

end
    
@test KDshow(KDHam(1,[])) == "h"
@test KDshow(KDHam(1,[]),TeX=true) == "h^{(0)}"

@test 2*KDHam(1,1)-KDSum(KDHam("j","j"),["j"]) == KDSum(2*KDHam(1,1),[]) - KDSum(KDHam("j","j"),["j"])


@testset "testing the symmetry reduction" begin
    
    @test KDpropto(KDSum(KDHam("j", "i", 1, 2), ["i", "j"]),KDSum(KDHam("i", "j", 1, 2), ["i", "j"]))
    @test KDpropto(KDSum(KDHam("j", "i", 1, 2), ["i", "j"]),-KDSum(KDHam("i", "j", 1, 2), ["i", "j"]))
    @test KDpropto(KDSum(KDHam("j", "i", "k","l"), ["i", "j"]),KDSum(KDHam("i", "j", "k","l"), ["i", "j"]))
    @test !KDpropto(KDSum(KDHam("j", "i", "k","l"), ["i", "j"]),KDSum(KDHam("i", "j", "l","k"), ["i", "j"]))
    
    
    @test KDSum(KDHam("j","i",1,2),["i","j"]) == KDSum(KDHam("i","j",1,2),["i","j"])
    
    @test KDSum(KDHam("j","i",1,2),["i","j"]) + KDSum(KDHam("i","j",1,2),["i","j"])  == 2*KDSum(KDHam("i","j",1,2),["i","j"])
    @test KDSum(KDHam("j","i",1,2),["i","j"]) - KDSum(KDHam("i","j",1,2),["i","j"])  ==0
    
    
    @test SymReduce(KDSum(KDHam("j","i",1,2),["i"])) == -KDSum(KDHam("i","j",1,2),["i"])
    @test SymReduce(KDSum(KDHam("i","j",1,2),["i","j"])) == 0
    @test SymReduce(KDSum(KDHam("k","l","i","j"),["i","j"])) == 0
    @test SymReduce(KDSum(KDHam("j","i",2,1),["i","j"])
                    +KDSum(KDHam("i","j",1,2),["i","j"])) == 0
    @test SymReduce(KDSum(KDHam("j","i",1,2),["i","j"])
                    -KDSum(KDHam("i","j",1,2),["i","j"])) ==  0
    
end

@testset "testing Reduction" begin
    
    @test SymReduce(KDHam("i","j")) == KDHam("i","j")
    @test SymReduce(KDHam("j","i")) == KDHam("j","i")
    @test SymReduce(KDHam("j","i","k","l")) == -KDHam("i","j","k","l")
    @test SymReduce(KDHam("i","j","l","k")) == -KDHam("i","j","k","l")
    @test SymReduce(KDHam("j","i","l","k")) == KDHam("i","j","k","l")
    @test SymReduce(KDHam("j","j","l","k")) == 0
    @test SymReduce(KDHam("j","i","l","l")) == 0
    
    
    
    
    @test KDSum(KDHam("j","i","k","l"),["i","j"]) == KDSum(KDHam("i","j","k","l"),["i","j"])
    @test (SymReduce(KDSum(KDHam("i","j","i","l"),["i","j"]))
           == KDSum(KDHam("i","j","i","l"),["i","j"]))
    @test (SymReduce(KDSum(KDHam("j","i","i","l"),["i","j"]))
           == -KDSum(KDHam("i","j","i","l"),["i","j"]))
    @test (SymReduce(KDSum(KDHam("i","j","l","i"),["i","j"]))
           == -KDSum(KDHam("i","j","i","l"),["i","j"]))
    @test (SymReduce(KDSum(KDHam("j","i","l","i"),["i","j"]))
           == KDSum(KDHam("i","j","i","l"),["i","j"]))
    @test SymReduce(KDSum(KDHam("j",2,"l","k"),["i","j","l","k"])) == KDSum(KDHam(2,"j","k","l"),["i","j","l","k"])
    @test SymReduce(KDSum(KDHam("j",2,"k","l"),["i","j"])) == -KDSum(KDHam(2,"j","k","l"),["i","j"])
end


@testset "testing Renamable indexes" begin
    @test FindRenamableIndexes([KDs("k"),KDs("l")],
                               [KDs("i"),KDs("j")]) == [[KDs("i"),KDs("k")] [KDs("j"),KDs("l")]]
    
    @test FindRenamableIndexes([KDs("k"),KDs("i")],
                               [KDs("i"),KDs("j")]) == transpose([KDs("j") KDs("k")])
    
    @test FindRenamableIndexes([KDs("k"),KDs("i"),KDs("l")],
                               [KDs("l"),KDs("i"),KDs("j")]) == transpose([KDs("j") KDs("k")])
    @test FindRenamableIndexes([KDs("k"),KDs("r"),KDs("l")],
                               [KDs("l"),KDs("i"),KDs("j")]) == [[KDs("i"),KDs("k")] [KDs("j"),KDs("r")]]
    @test FindRenamableIndexes([KDs("k"),KDs("r"),KDs("l")],
                               [KDs("t"),KDs("i"),KDs("j")]) == [[KDs("t"),KDs("k")] [KDs("i"),KDs("r")] [KDs("j"),KDs("l")]]
end


@testset "testing Ham reorder function" begin
    @test (HamReorderFunction(KDHam("k","l"),
                              KDHam("l","k"),
                              [KDs("k")]) == [[] []])
    @test (HamReorderFunction(KDHam("k","l"),
                              KDHam("k","l"),
                              [KDs("k"),KDs("l")]) == [[] []])
    @test (HamReorderFunction(KDHam("k","l"),
                              KDHam("l","k"),
                              [KDs("k"),KDs("l")]) ==
           [[KDs("l"),KDs("k")] [KDs("k"),KDs("l")]])
    @test (HamReorderFunction(KDHam("i","k","l"),
                              KDHam("i","l","k"),
                              [KDs("i"),KDs("k"),KDs("l")]) ==
           [[KDs("l"),KDs("k")] [KDs("k"),KDs("l")]])
    @test (HamReorderFunction(KDHam("i","k","l"),
                              KDHam("l","i","k"),
                              [KDs("i"),KDs("k"),KDs("l")]) ==
           transpose([[KDs("l"),KDs("i")] [KDs("i"),KDs("k")] [KDs("k"),KDs("l")]]))
    @test (HamReorderFunction(KDHam("i","k","l"),
                              KDHam("l","i","k"),
                              [KDs("k"),KDs("l")]) ==
           [[KDs("l"),KDs("k")] [KDs("k"),KDs("l")]])
    @test (HamReorderFunction(KDHam("i","k","l"),
                              KDHam("l","j","k"),
                              [KDs("k"),KDs("l")]) ==
           [[KDs("l"),KDs("k")] [KDs("k"),KDs("l")]])
    @test (HamReorderFunction(KDHam("i","k","l"),
                              KDHam("j","l","k"),
                              [KDs("k"),KDs("l")]) ==
           [[KDs("l"),KDs("k")] [KDs("k"),KDs("l")]])
    @test (HamReorderFunction(KDHam("i","k","l"),
                              KDHam("j","l","k"),
                              [KDs("k")]) == [[] []])
    @test (HamReorderFunction(KDHam("k","k","l"),
                              KDHam("l","k","k"),
                              [KDs("k")]) == [[] []])
    @test (HamReorderFunction(KDHam("k","k","l"),
                              KDHam("l","k","k"),
                              [KDs("k"),KDs("l")]) ==
           [[KDs("l"),KDs("k")] [KDs("k"),KDs("l")]])
    @test (HamReorderFunction(KDHam("k","l","l","k"),
                              KDHam("l","k","k","l"),
                              [KDs("k"),KDs("l")]) ==
           [[KDs("l"),KDs("k")] [KDs("k"),KDs("l")]])
end


@test KDHamPermIndx(KDHam("i","j"),[[KDs("i"),KDs("j")] [KDs("j"),KDs("i")]]) == KDHam("j","i")
@test KDHamPermIndx(KDHam("i","j","k"),[[KDs("i"),KDs("j")] [KDs("j"),KDs("i")]]) == KDHam("j","i","k")
@test KDHamPermIndx(KDHam("i","l","k"),
                    collect(transpose([[KDs("l"),KDs("i")] [KDs("i"),KDs("k")] [KDs("k"),KDs("l")]]))) == KDHam("k","i","l")


SumList=[KDSum(KDHam("i","j","k","l")*KD("i","l")*KD("j","k"),["i","j","k","l"]),
         KDSum(KDHam("i","j","k","l")*KD("i","j")*KD("l","k"),["i","j","k","l"]),
         KDSum(KDHam("i","j","k","l")*KD("i","k")*KD("l","j"),["i","j","k","l"])]
TargetList=[KDSum(KDHam("i","j","j","i"),["i","j"]),
            KDSum(KDHam("i","i","l","l"),["i","l"]),
            KDSum(KDHam("i","j","i","j"),["i","j"])]

for i1 in 1:3
    for i2 in 1:3
        println("-----  $i1 vs $i2  ------")
        if i1==i2
            @test SumList[i1] == TargetList[i2]
        else
            @test SumList[i1] != TargetList[i2]
        end
    end
end



@testset "testing proportinality" begin
    @test KDpropto(KDHam(1,2),KDHam(1,2))
    @test KDpropto(KDHam(1,2),2*KDHam(1,2))
    @test !KDpropto(KDHam(1,2),KDHam(2,1))
    @test !KDpropto(KDHam(1,2,3),KDHam(1,2))
    
    
    @test KDpropto(KD(1,2),KD(2,1))
    @test KDpropto(KD(1,2),KD(1,2))
    @test !KDpropto(KD(1,2),KD(3,1))
    @test_throws ErrorException !KDpropto(KD(1,"2"),KD("3",1))
    @test !KDpropto(KD(1,2),KD(3,1))
    
    
    @test KDpropto(KDHam(1,2)*KD(1,2),KDHam(1,2)*KD(2,1))
    @test !KDpropto(KDHam(1,2)*KD(1,2),KDHam(1,2)*KD(1,"j"))
    @test !KDpropto(KDHam(1,2)*KD(1,2),KDHam(1,2)*KD(1,2)*KD(1,"j"))
    @test !KDpropto(KDHam("i","j")*KD(1,2)*KD(1,"j"),KDHam("i","j")*KD(1,2))
end

@testset "testing hamiltonian relations" begin
    @test  -KDHam(1,2) != KDHam(1,2)
    @test  -KDHam("i",2) != KDHam("j",2)
    
    ###Check hamiltonian comparrison
    @test  KDHam(1) < KDHam(2)
    @test  KDHam(2) < KDHam(1,1)
    @test  KDHam(1) < KDHam(2)
    @test  1*KDHam(1) < 2*KDHam(1)
    @test  -KDHam(1) < KDHam(1)
    @test  KDHam(1) < -KDHam(1,2)
    @test  KDHam("i","j") < KDHam("i","k")
    @test  KDHam("i","k") < KDHam("j","i")
    @test  KDHam("i","j","k") < KDHam("i","k","j")
    
    @test KDHam("i","j")*(KD(1,"i")-KD(1,"j")) == KDHam("i","j")*KD(1,"i") - KDHam("i","j")*KD(1,"j")
end



###Test that copies of the hamiltonians is craeted 
KDtmp=KDHam("i","j")*MajIP(["i",1,2,"j"])
@test  KDtmp.kdhpList[1].ham != KDtmp.kdhpList[2].ham
@test  KDtmp.kdhpList[1].ham.IndxList == KDtmp.kdhpList[2].ham.IndxList
KDtmp.kdhpList[1].ham.IndxList[1]=KDs("r")
@test  KDtmp.kdhpList[1].ham.IndxList != KDtmp.kdhpList[2].ham.IndxList

@test KDSum(KDHam("i","j")*MajIP(["i",1,2,"j"]),["i","j"]) == KDHam(1,2)-KDHam(2,1)

@test KDSum(KDHam("i")*KD(1,"k"),"i") == KDSum(KDHam("j")*KD("k",1),"j")
@testset "tseting hamiltonian equivalance" begin
    @test Kdelta("i","j")*Kdelta("i","j")==Kdelta("i","j")
    @test Kdelta(1,"j")*Kdelta("j",1)==Kdelta(1,"j")
    
    @test KDHam("i") == KDHam("i")
    @test KDHam("i") != KDHam("j")
    @test KDHam("i","j") != KDHam("j","i")
    @test KDHam("i","j") == KDHam("i","j")
    
    @test KDHam("i")*Kdelta("j","k") == Kdelta("j","k")*KDHam("i")
    @test (2*KDHam("i"))*Kdelta("j","k") == (2*Kdelta("j","k"))*KDHam("i")
    
    
    @test KDSum(KDHam("i")*Kdelta("i",1),"i") == KDHam(1)
    @test KDSum(KDHam("i","i")*Kdelta("i",1),"i") == KDHam(1,1)
    
    
    @test KDprodIndexes(Kdelta("i",1)*Kdelta("i",2)) == [KDIndex(y) for y in [1,2,"i"]]
    @test KDprodIndexes(Kdelta("i","j")*Kdelta("i","j")) == [KDIndex(y) for y in ["i","j"]]
    
    @test KDSumExternalIndexes(KDSum(KDHam(1,"j","k","l"),["j","k"])) == [KDIndex(y) for y in [1,"l"]]
    @test KDSumExternalIndexes(KDSum(KDHam(1,"i")*Kdelta("j","k"),["i"])) == [KDIndex(y) for y in [1,"j","k"]]
    
    
    @test KDSum(KDHam("i","k"),"i") !== KDSum(KDHam("i","l"),"i")
    @test KDSum(KDHam("i","i"),"i") == KDSum(KDHam("i","i"),"i")
    @test KDSum(KDHam("i"),"i") !== KDSum(KDHam("i","i"),"i")
    @test KDSum(KDHam("i")*KD("j","k"),"i") !== KDSum(KDHam("i")*KD("l","k"),"i")
    @test KDSum(KDHam("i")*KD(1,"k"),"i") == KDSum(KDHam("j")*KD("k",1),"j")
    @test KDSum(KDHam("i","i"),"i") == KDSum(KDHam("j","j"),"j")
    @test KDSum(KDHam("i","j"),["j","i"]) == KDSum(KDHam("j","i"),["i","j"])
    @test KDSum(KDHam("i","j"),["j"]) != KDSum(KDHam("j","i"),["i"])
    
    
    @test KDSum(KDHam("i","j")*Kdelta("i","j"),["i","j"]) == KDSum(KDHam("i","i"),"i")
    
end
