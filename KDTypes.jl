abstract type KDAllTypes end ###Adding A special overarchiong type so simplify some expressions




abstract type KDType <: KDAllTypes end ###Adding A special overarchiong type so simplify some expressions


abstract type KDIndex <: KDAllTypes end ###Creating a special index type for the KD
struct KDi <: KDIndex; x::Integer; end
struct KDs <: KDIndex; x::String; end
KDIndex(x::Integer) = KDi(x)
function KDIndex(x::String)
    xi = tryparse(Float64,x)
    if xi == nothing
        return KDs(x)
    elseif isinteger(xi)
        error("The index $x represents an integer. This should not be given as a string, but a sa pute integer")
    else
        error("The index $x represents a float that is not an integer. Cannot be represented propperly.")
    end
end
KDIndex(x::KDi)  = x
KDIndex(x::KDs)  = x



struct KD <: KDType; x::KDIndex; y::KDIndex; end
KD(x::Integer,y::Integer) = if x < y ; KD(KDIndex(x),KDIndex(y)) else KD(KDIndex(y),KDIndex(x)) end
KD(x::Integer,y::String) = KD(KDIndex(x),KDIndex(y))
KD(y::String,x::Integer) = KD(KDIndex(x),KDIndex(y))
KD(x::String,y::String) = if x < y ; KD(KDIndex(x),KDIndex(y)) else KD(KDIndex(y),KDIndex(x)) end

#struct KDis <: KD; x::Integer; y::String; end
#struct KDss <: KD; x::String; y::String; end

struct KDprod <: KDType; KDList::Array{KD,1}; scale::Integer; end
KDprod(x::KD) = KDprod([x],1)
KDprod() = KDprod([],1)
###Here sp stands for super position
struct KDSupPos <: KDType ; x::Integer;kdpl::Array{KDprod,1}; end




###Adding A special overarchiong type so simplify some expressions
abstract type KDHamType <: KDAllTypes end
###Creating some concrete types under the abstract type KD
mutable struct KDHam <: KDHamType; scale::Integer; IndxList::Array{KDIndex,1}; end
KDHam() = KDHam(1,[])
KDHam(x::Integer) = KDHam(KDIndex(x))
KDHam(x::String)  = KDHam(KDIndex(x))
KDHam(x::KDIndex) = KDHam(1,[x])
KDHam(x::KDIndex,y::KDIndex)= KDHam(1,[x,y])
KDHam(x::Integer,y::Integer) = KDHam(KDIndex(x),KDIndex(y))
KDHam(x::Integer,y::String) = KDHam(KDIndex(x),KDIndex(y))
KDHam(x::String, y::Integer)= KDHam(KDIndex(x),KDIndex(y))
KDHam(x::String, y::String) = KDHam(KDIndex(x),KDIndex(y))
KDHam(x,y,z) = KDHam(1,[KDIndex(x),KDIndex(y),KDIndex(z)])
KDHam(x,y,z,w) = KDHam(1,[KDIndex(x),KDIndex(y),KDIndex(z),KDIndex(w)])

### A struch for products of hmiltonians and delta products
mutable struct KDHamProd <: KDHamType; ham::KDHam; prod::KDprod; end
KDHamProd(x::KDHam) = KDHamProd(x,1)
### A structure for indfiite sums over hemiltonians
mutable struct KDSumHamProd <: KDHamType; ham::KDHam; prod::KDprod; IndxList::Array{KDIndex,1} end
KDSumHamProd(kd::KDHamProd)=KDSumHamProd(kd.ham,kd.prod,[])

struct KDHamProdSupPos <: KDHamType; kdhpList::Array{KDHamProd}; end
struct KDSumHamProdSupPos <: KDHamType; kdshpList::Array{KDSumHamProd}; end


