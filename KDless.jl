


#### Inequalities
import Base.<     #### less than ###
import Base.isless     #### less than ###
isless(x::KDIndex,y::KDIndex) = KDless(x,y)
isless(x::KDHamType,y::KDHamType) = KDless(x,y)
isless(x::KDprod,y::KDprod) = KDless(x,y)

<(x::KDIndex,y::KDIndex) = KDless(x,y)
<(x::KD,y::KD) = KDless(x,y)
<(x::KDprod,y::KDprod) = KDless(x,y)

###The indernal order is KDii < KDis < KDss
KDless(a::KDi,b::KDi)= a.x < b.x
KDless(a::KDs,b::KDs)= a.x < b.x
KDless(a::KDi,b::KDs)= true
KDless(a::KDs,b::KDi)= false
    
function KDless(a::KD,b::KD)
    if a.x < b.x
        return true
    elseif a.x > b.x
        return false
    elseif a.x == b.x
        if a.y < b.y
            return true
        elseif a.y > b.y
            return false
        elseif a.y == b.y
            return false
        end
    end
    error("Broke out of if-maze")
end
function KDless(a::KDprod,b::KDprod)
    ###Here we order the prducts bases on their list of KDs
    ###Smaller lists got first
    KLa=KDreduce!(a.KDList)
    KLb=KDreduce!(b.KDList)
    if length(KLa) < length(KLb)
        return true
    elseif length(KLa) > length(KLb)
        return false
    else ##If the lengths are the same check which one has the smaller start elements
        for indx in 1:length(KLa)
            if KLa[indx] < KLb[indx]
                return true
            elseif KLa[indx] > KLb[indx]
                return false
            end
            ###If no element is smaller (or bigger) they are equal, and thus a is not smaller
        end
        return false
    end
    error("End of if-maze")
end


function KDless(x::KDHamProd,y::KDHamProd)
    if x.ham < y.ham
        return true
    elseif y.ham < x.ham
        return false
    elseif y.ham == x.ham
        ###Check prod
        if x.prod < y.prod
            return true
        elseif y.prod < x.prod
            return false
        elseif y.prod == x.prod
            false
        else
            error("Should not happen")
        end
    else
        error("Should not happen")
    end
end

function KDless(x::KDHam,y::KDHam)
    if length(x.IndxList) < length(y.IndxList)
        return true ##Shorter hamiltonians first
    elseif length(y.IndxList) < length(x.IndxList)
        return false
    else ##Same length check the index ordering
        #println("x:",x)
        #println("y:",y)
        for indx in 1:length(x.IndxList)
            if x.IndxList[indx] < y.IndxList[indx]
                return true
            elseif y.IndxList[indx] < x.IndxList[indx]
                return false
            end
        end
        ###Neither bigger or smaller
        return x.scale < y.scale
    end
end



function KDless(x::KDSumHamProd,y::KDSumHamProd)
    if length(x.IndxList) < length(y.IndxList)
        return true
    elseif length(y.IndxList) < length(x.IndxList)
        return false
    elseif x.ham < y.ham
        return true
    elseif y.ham < x.ham
        return false
    elseif y.ham == x.ham
        ###Check prod
        if x.prod < y.prod
            return true
        elseif y.prod < x.prod
            return false
        elseif y.prod == x.prod
            false
        else
            error("Should not happen")
        end
    else
        error("Should not happen")
    end
end

