
if !@isdefined(TabLevel)
    TabLevel = ""
end
println(TabLevel*"Open KD.jl")
TabLevel=TabLevel*"    "



include("KDTypes.jl")
include("KDadd.jl")
include("KDmul.jl")
include("KDequal.jl")
include("KDless.jl")
include("KDprint.jl")
include("KDcopy.jl")
include("KDpropto.jl")



### Converting KD to KDprod
import Base.convert
convert(::Type{KDprod}, x::KD) = KDprod(x)
convert(::Type{KDHamProd}, x::KDHam) = KDHamProd(x,KDprod())



KDreduce!(a::KD , b::KD ) = KDreduce!([a,b])
KDreduce!(b::KD, KDList::Array{KD,1}) = KDreduce!(KDList,b)
KDreduce!(KDList::Array{KD,1},b::KD)  = KDreduce!([KDList;b])
KDreduce!(a::Array{KD,1},b::Array{KD,1})  = KDreduce!([a;b])
function KDreduce!(KDList::Array{KD,1};verbose=false)
    NumElems=length(KDList)
    CheckStep=NumElems-1
    while CheckStep>0
        verbose && println("CheckStep=",CheckStep," of ",NumElems)
        if CheckStep==NumElems
            verbose && println("moved to faar to right")
            ###Move to the left and continue
            CheckStep-=1
            continue
        end
        ElemA=KDList[CheckStep]
        ElemB=KDList[CheckStep+1]
        verbose && println("ElemA:",ElemA," ElemB:",ElemB)
        if ElemA==ElemB ###If the elements are the same remove of them
            ###Remove one of the elements
            deleteat!(KDList,CheckStep)
            NumElems-=1
            verbose && println("remove pair")
        elseif ElemA < ElemB
            ###Ordered move to the left and continute
            verbose && println("Correctly ordered")
            CheckStep-=1
            continue
        else# ElemA > ElemB
            KDList[CheckStep]=ElemB
            KDList[CheckStep+1]=ElemA
            verbose && println("Reverse order")
            CheckStep+=1
            continue
        end
        CheckStep-=1
    end
    return KDList
end

function KDSort(kd::KD)
    if kd.x < kd.y
        return KD(kd.x,kd.y)
    else
        return KD(kd.y,kd.x)
    end
end
KDsort!(a::KDprod , b::KDprod ) = KDsort!([a,b])
KDsort!(b::KDprod, KDList::Array{KDprod,1}) = KDsort!(KDList,b)
function KDsort!(KDList::Array{KDprod,1};verbose=false)
    verbose && println("  ============================  ")
    NumElems=length(KDList)
    CheckStep=NumElems-1
    while CheckStep>0
        verbose && println("CheckStep=",CheckStep," of ",NumElems," elements")
        verbose && println("length(KDList) = ",length(KDList))
        if CheckStep==NumElems
            verbose && println("moved to faar to right")
            ###Move to the left and continue
            CheckStep-=1
            continue
        end
        ElemA=KDList[CheckStep]
        ElemB=KDList[CheckStep+1]
        verbose && println("ElemA:",ElemA," ElemB:",ElemB)
        if KDPropto!(ElemA,ElemB) ###If the elements are proprtional add them together
            ###Remove one of the elements
            SumElem=ElemA+ElemB
            deleteat!(KDList,CheckStep)
            if SumElem==0
                deleteat!(KDList,CheckStep)
                NumElems-=2
            else
                KDList[CheckStep]=SumElem
                NumElems-=1
            end
            verbose && println("remove pair")
        elseif ElemA < ElemB
            ###Ordered move to the left and continute
            verbose && println("Correctly ordered")
            CheckStep-=1
            continue
        else# ElemA > ElemB
            KDList[CheckStep]=ElemB
            KDList[CheckStep+1]=ElemA
            verbose && println("Reverse order")
            CheckStep+=1
            continue
        end
        CheckStep-=1
    end
    return KDList
end



KDPropto!(kdpa::KDprod,kdpb::KDprod) = KDPropto!(kdpa.KDList,kdpb.KDList)
function KDPropto!(KDL1::Array{KD,1},KDL2::Array{KD,1})
    KDL1=KDreduce!(KDL1) ###Sort the arrays
    KDL2=KDreduce!(KDL2) ###Sort the arrays
    #println("Checking the lists prodcuts")
    LenA=length(KDL1)
    LenB=length(KDL2)
    #println("LenA = $LenA")
    #println("LenB = $LenB")
    if LenA != LenB
        return false
    else
        for indx in 1:LenA
            if KDL1[indx] != KDL2[indx]
                return false
            end
        end
        ### IF all this are equal return true
        return true
    end
end
    
KDTerms(x::Integer,y::KD) = KDTerms(x,KDprod(y))
KDTerms(x::Integer,y::KDprod) = KDTerms(x,[y])
KDTerms(x::Integer,y::KDSupPos) = KDTerms(x+y.x,y.kdpl)
function KDTerms(x::Integer,y::Array{KDprod,1})
    if length(y) == 0
        return x
    else
        tmp=KDSupPos(x,KDsort!(y))
        if x==0 && length(tmp.kdpl)==1
            1 ###If only one term, reduve to KDprod
            return tmp.kdpl[1]
        elseif length(tmp.kdpl)==0
            return x
        else
            return tmp
        end
    end
end




MajIP(x::Integer) = 0
MajIP(x::String) = 0
function MajIP(x::Array;verbose=false)
    ####Maj IP computes the expectation value of an operator
    ###If the indexs are taken to be integers they are considered fixed
    ### And if the integers a re diffetrnt they are different indexes
    ### Indexes that a re strings, characters are considered as free floating indexes, and will apear in Kroenecker deltas
    verbose && println("contracting $x")
    NumElems=length(x)
    if mod(NumElems,2)==1
        return 0
    elseif NumElems==0
        return 1
    elseif NumElems==2
        verbose && println("Two elements")
        return Kdelta(x[1],x[2],verbose=verbose)
    else
        verbose && println("More than two elements")
        if x[1] == x[2]
            return MajIP(x[3:end])
        else
            Tot=0
            Sign=1
            for indx in 2:NumElems
                verbose &&  println("Loking at pair (1,$indx)")
                tmp=Sign*Kdelta(x[1],x[indx],verbose=verbose)
                if tmp!=0
                    Indxes=[collect(2:(indx-1));collect((indx+1):NumElems)]
                    verbose &&  println("Indxes for $indx (of $NumElems):, $Indxes")
                    tmp=tmp*MajIP(x[Indxes],verbose=verbose)
                    verbose && println("tmp = $tmp")
                    Tot = Tot + tmp
                end
                verbose && println("tmp = $tmp")
                verbose && println("Tot = $Tot")
                Sign *= -1
            end
            return Tot
        end
    end
    error("Reach end of if-maze?")
end


function Kdelta(x::Integer,y::Integer;verbose=false)
    verbose && println("KD of integers $x and $y")
    if x==y
        return 1
    else
        return 0
    end
end
function Kdelta(x::String,y::String;verbose=false)
    verbose && println("KD of strings $x and $y")
    if x==y
        return 1
    elseif x < y
        verbose && println("$x < $y")
        return KD(x,y)
    else
        verbose && println("$x > $y")
        return KD(y,x)
    end
end
Kdelta(y::String,x::Integer;verbose=false) = Kdelta(x,y;verbose=verbose)
function Kdelta(x::Integer,y::String;verbose=false)
    verbose && println("KD of integer $x and string $y")
    return KD(x,y)
end




###Conversions
convert(::Type{KDprod}, x::Integer) = KDprod([],1)


function KDHamSwapablePair(x::KDSumHamProd,y::KDSumHamProd,pos1,pos2;verbose=false)
    ###Check if the indexes on the hamiltonian corresponds to pair that can be swapped

    x1=x.ham.IndxList[pos1]
    x2=x.ham.IndxList[pos2]
    y1=y.ham.IndxList[pos1]
    y2=y.ham.IndxList[pos2]
    XI = x.IndxList
    YI = y.IndxList
    verbose && println("x1,x2,y1,y2:",x1,x2,y1,y2)
    if x1 == y1 && x2 == y2
        return false ###Not swappable, first indexes the same
    elseif x1 == y2 && x2 == y1
        ###Swappable since indexes coincide
        ###But also need to be in the free index list
        if (x1 in XI) && (x2 in XI) && (x1 in YI) && (x2 in YI)
            ###The indexes are in the lists
            return true
        else
            return false
        end
    else
        ###The index are nor swappable
        return false
    end
end


function KDProdSubIndx(kd::KD,IndxFrom::KDIndex,IndxTo::KDIndex;
                       verbose=false)
    if kd.x == IndxFrom
        x2= IndxTo
    else
        x2= kd.x
    end
    if kd.y == IndxFrom
        y2= IndxTo
    else
        y2= kd.y
    end
    return KD(x2.x,y2.x)
end


function KDProdSubIndx(prod::KDprod,IndxFrom::KDIndex,IndxTo::KDIndex;
                      verbose=false)
    verbose && println("prod in: ",KDshow(prod))
    verbose && println("IndxFrom:",IndxFrom)
    verbose && println("IndxTo:",IndxTo)
    ##Loop over the indexes and change out the relevant ones
    for INo in 1:length(prod.KDList)
        prod.KDList[INo] = KDProdSubIndx(prod.KDList[INo],IndxFrom,IndxTo)
    end
    verbose && println("prod out: ",KDshow(prod))
    return prod
end


function KDHamSubIndx(ham::KDHam,IndxFrom::KDIndex,IndxTo::KDIndex;
                      verbose=false)
    verbose && println("ham in: ",KDshow(ham))
    verbose && println("IndxFrom:",IndxFrom)
    verbose && println("IndxTo:",IndxTo)
    ##Loop over the indexes and change out the relevant ones
    for INo in 1:length(ham.IndxList)
        if ham.IndxList[INo] == IndxFrom
            ham.IndxList[INo] = IndxTo
        end
    end
    verbose && println("ham out: ",KDshow(ham))
    return ham
end



function KDSumSubIndx(kd::KDSumHamProd,IndxFrom::KDIndex,IndxTo::KDIndex;
                   verbose=false)
    verbose && println("kd in:",KDshow(kd))
    verbose && println("IndxFrom:",IndxFrom)
    verbose && println("IndxTo:",IndxTo)
    ##Loop over the indexes and change out the relevant ones
    for INo in 1:length(kd.IndxList)
        if kd.IndxList[INo] == IndxFrom
            kd.IndxList[INo] = IndxTo
        end
    end
    verbose && println("after kd indexes:",KDshow(kd))
    kd.ham = KDHamSubIndx(kd.ham,IndxFrom,IndxTo)
    verbose && println("after ham indexes:",KDshow(kd))
    kd.prod = KDProdSubIndx(kd.prod,IndxFrom,IndxTo)
    verbose && println("kd out:",KDshow(kd))
    return kd
end



function KDHamSwapIndx(ham::KDHam,IndxFrom::KDIndex,IndxTo::KDIndex;verbose=false)
    verbose && println("ham in:",ham)
    verbose && println("IndxFrom:",IndxFrom)
    verbose && println("IndxTo:",IndxTo)
    ##Loop over the indexes and swap all the indexes
    for INo in 1:length(ham.IndxList)
        if ham.IndxList[INo] == IndxFrom
            ham.IndxList[INo] = IndxTo
        elseif ham.IndxList[INo] == IndxTo
            ham.IndxList[INo] = IndxFrom
        end
    end
    verbose && println("ham out:",ham)
    return ham
    end

KDisZero(x::KDHamProd) = KDisZero(x.ham) || KDisZero(x.prod)
KDisZero(x::KDSumHamProd) = KDisZero(x.ham) || KDisZero(x.prod)
KDisZero(x::KDHam) = x.scale==0
KDisZero(x::KDprod) = x.scale==0
KDisZero(x::Integer) = x==0


#####A function implementeing KDSum
KDSum(x::KDHamProd,y)=KDSum(x,KDIndex(y))
KDSum(x::KDHamProd,y::KDIndex) =KDSum(x,[y])
KDSum(x::KDHam,y::Array) =KDSum(KDHamProd(x),y)
KDSum(x::KDHam,y::String) =KDSum(KDHamProd(x),KDIndex(y))
KDSum(x::KDHam,y::Integer) =KDSum(KDHamProd(x),KDIndex(y))
KDSum(x::KDHam,y::KDIndex) =KDSum(KDHamProd(x),[y])

function KDSum(x::Integer,IndxList::Array;verbose=false)
    if x==0
        return 0
    elseif length(IndxList)==0
        return x
    else
        !verbose && println("x:",x)
        !verbose && println("IndxList:",IndxList)
        error("FIXME: Not implemented")
    end
end
    
function KDSum(x::KDHamProd,IndxList::Array;verbose=false)
    verbose && println("---------KDSum---------")
    verbose && println("IndxList:",IndxList)
    if (typeof(IndxList)==Array{KDIndex,1} ||
        typeof(IndxList)==Array{KDi,1} ||
        typeof(IndxList)==Array{KDs,1} )
        verbose && println("Acceptable type")
    else
        verbose && println("Convert to acceptable array")
        IndxList=[KDIndex(y) for y  in  IndxList]
        #error("Wrong type = $(typeof(IndxList)), expected Array{KDIndex,1}")
    end
    verbose && println("IndxList:",IndxList)
    verbose && println("KDHamProd: ",KDshow(x))
    Ham=x.ham
    Deltas=x.prod.KDList
    verbose && println("Ham: ",Ham)
    verbose && println("Deltas: ",Deltas)
    IndxY=1
    while IndxY <= length(IndxList)
        ###Check if the index is present in the hamiltonian and/or the the delta products
        TargetIndx=IndxList[IndxY]
        verbose && println("Index no $IndxY: ",TargetIndx)


        IndexMatch,SwapIndex = KDFindMatchingIndex(Deltas,TargetIndx)

        if IndexMatch
            ###Substitute the index the Hamiltonain
            Ham=KDIndxSub(Ham,TargetIndx,SwapIndex)
            deleteat!(IndxList,IndxY)
            IndxY-=1 ###Set counter one step back
        end
        IndxY+=1 ###Look at next index
    end
    if length(Deltas)==0 && length(IndxList)==0
        return Ham
    else
        return KDSumHamProd(Ham,KDprod(Deltas,1),IndxList)
    end
end


function KDSum(x::KDHamProdSupPos,IndxList::Array;verbose=false)
    ###Apply the sum to each of the terms of the super postion
    verbose && println("  -.-.-.-.-.-.-.-.-.  ")
    verbose && println("KDSum Input: ", KDshow(x))
    Tot = 0
    for indx in 1:length(x.kdhpList)
        Term=x.kdhpList[indx]
        verbose && println("Term[$indx]:",KDshow(Term))
        tmp = KDSum(Term,IndxList)
        verbose && println("tmp[$indx]:",KDshow(tmp))
        Tot = Tot + tmp
        verbose && println("Tot[$indx]:",KDshow(Tot))
    end
    return Tot
end


function KDFindMatchingIndex(Deltas::Array{KD,1},TargetIndx::KDIndex;verbose=false)        
    ##Check if the index is present in the delta functions
    IndxP=1
    while IndxP <= length(Deltas)
        verbose && println("Delta Index no $IndxP: ",Deltas[IndxP])
        ###Check is the index is present
        if KDIndexMatch(Deltas[IndxP],TargetIndx)
            verbose && println("Found match in  $TargetIndx")
            xindx=Deltas[IndxP].x
            yindx=Deltas[IndxP].y
            if  xindx == yindx
                error("Indexes are the same $(yindx)!remove the DK")
            elseif xindx == TargetIndx
                SwapIndex = yindx
            elseif yindx == TargetIndx
                SwapIndex = xindx
            end
            ### Remove the current index as not needed
            deleteat!(Deltas,IndxP)
            IndxP-=1
            
            return true, SwapIndex
        end
        IndxP+=1
    end
    return false, 0
end


    

function KDIndexMatch(kd::KD,TargetIndx::KDIndex)
    return kd.x == TargetIndx || kd.y ==TargetIndx
end


function KDIndxSub(Ham::KDHam,TargetIndx::KDIndex,SwapIndex::KDIndex)
    #println("Making Swap in Ham=$Ham")
    #println("Ham.IndxList=",Ham.IndxList)
    #println("TargetIndx=$TargetIndx")
    #println("SwapIndex=$SwapIndex")
    for i in 1:length(Ham.IndxList)
        #println("Ham.IndxList[$i]=",Ham.IndxList[i])
        if Ham.IndxList[i] == TargetIndx
            #println("Match")
            Ham.IndxList[i] = SwapIndex
        end
    end
    #println("After Swap Ham=$Ham")
    return Ham
end


function KDSumExternalIndexes(x::KDSumHamProd)
    ####Worl out which indexes are external
    ##Find all indexes listed in Ham
    #println("----------------- Find external indexes -------------")
    HamIndx=x.ham.IndxList
    #println("HamIndx:", HamIndx)
    UniqueHamIndx=unique(HamIndx)
    #println("UniqueHamIndx:", UniqueHamIndx)
    KDIndexes=KDprodIndexes(x.prod)
    #println("KDIndexes:", KDIndexes)
    PhonyIndexes=unique(x.IndxList)
    #println("PhoneIndexes:", PhonyIndexes)
    ###Filter out the elements that are Phony
    filter!(e->!(e in PhonyIndexes),[UniqueHamIndx ; KDIndexes])
end



KDprodIndexes(kd::KD)=KDprodIndexes(KDprod(kd))
function KDprodIndexes(kdp::KDprod)
    ###Loop over all the delta fuctions and collect all the indexes
    #println("kdp: ", kdp)
    IndxList=Array{KDIndex,1}(undef,length(kdp.KDList)*2)
    for indx in 1:length(kdp.KDList)
        IndxList[2*indx-1]=kdp.KDList[indx].x
        IndxList[2*indx-0]=kdp.KDList[indx].y
    end
    #println("IndxList: ", IndxList)
    return unique(sort(IndxList))
end



function ValidateKDSum(x::KDSumHamProd)
    if !(unique(sort(x.IndxList)) == sort(x.IndxList))
        error("The sum ",KDshow(x)," sums over ",KDshow(x.IndxList)," and contains the same index twice")
    end
end

import  Base.transpose
transpose(x::KDs)=x




function HamReorderFunction(hamX::KDHam,hamY::KDHam,IndxList::Array;
                            verbose=false)
    IndxList = [KDIndex(x) for x in IndxList]
    verbose && println("........................:")
    verbose && println("hamX: ",KDshow(hamX))
    verbose && println("hamY: ",KDshow(hamY))
    verbose && println("IndxList:",KDshow(IndxList))
    if length(hamX.IndxList) != length(hamY.IndxList)
        error("Hamiltonians are of different sizes.")
    end
    MatchingIndexes=copy(sort(IndxList))
    TmpXIndx=KDcopy(hamX.IndxList)
    TmpYIndx=KDcopy(hamY.IndxList)
    verbose && println("TmpXIndx: ",KDshow(TmpXIndx))
    verbose && println("TmpYIndx: ",KDshow(TmpYIndx))
    ##Drop indexes taht are not summed over
    filter!(e->(e in MatchingIndexes),TmpXIndx)
    filter!(e->(e in MatchingIndexes),TmpYIndx)
    verbose && println("cleaned TmpXIndx: ",KDshow(TmpXIndx))
    verbose && println("cleaned TmpYIndx: ",KDshow(TmpYIndx))
    
    ###Loop over the indexes to find the appropriate permutation of the indexes
    ##

    PermList=fill(KDs("i"),0,2)
    verbose && println("PermList: ",PermList)
    while length(TmpXIndx) > 0  && length(TmpYIndx) > 0
        XIndx=TmpXIndx[1]
        YIndx=TmpYIndx[1]
        if XIndx == YIndx
            ###The Indexes are the same, remove them from further anaysis
            ###Do nothing here
        else
            #Extent the perm list with [YIndx,Xindx]
            PermList = [ PermList ; [YIndx XIndx] ]
            verbose && println("PermList: ",PermList)
        end
        filter!(e->!(e in [XIndx]),TmpXIndx)
        filter!(e->!(e in [YIndx]),TmpYIndx)
        verbose && println("cleaned TmpXIndx: ",KDshow(TmpXIndx))
        verbose && println("cleaned TmpYIndx: ",KDshow(TmpYIndx))
    end
    ###Loop over the indexes and change out the relevant ones
    #for INo in 1:length(ham.IndxList)
    #    if ham.IndxList[INo] == IndxFrom
    #        ham.IndxList[INo] = IndxTo
    #    end
    #end
    #verbose && println("ham out: ",KDshow(ham))
    #return ham
    verbose && println("Final PermList: ",PermList)
    return PermList
end



function KDHamPermIndx(y::KDHam,ReorderMap::Array)
    (PL,PW) = size(ReorderMap)
    if PW != 2
        error("The reorder map shoudl be (n,2) but is $(size(ReorderMap))")
    end
    for indx in 1:length(y.IndxList)
        for p2 in 1:PL
            if y.IndxList[indx] == ReorderMap[p2,1]
                y.IndxList[indx] = ReorderMap[p2,2]
                break
            end
        end
    end
    return y
end
function KDProdPermIndx(y::KDprod,ReorderMap::Array)
    (PL,PW) = size(ReorderMap)
    if PW != 2
        error("The reorder map shoudl be (n,2) but is $(size(ReorderMap))")
    end
    for indx in 1:length(y.KDList)
        if y.KDList[indx] == KDProdPermIndx(y.KDList[indx],ReorderMap)
        end
    end
    return y
end

function KDProdPermIndx(kd::KD,ReorderMap::Array)
    (PL,PW) = size(ReorderMap)
    if PW != 2
        error("The reorder map shoudl be (n,2) but is $(size(ReorderMap))")
    end
    Xnew=kd.x
    Ynew=kd.y
    for indx in 1:PL
        if kd.x == ReorderMap[indx,1]
            Xnew = ReorderMap[indx,2]
            break
        end
    end
    for indx in 1:PL
        if kd.y == ReorderMap[indx,1]
            Ynew = ReorderMap[indx,2]
            break
        end
    end
    return KD(Xnew.x,Ynew.x)
end


SymReduce(x::Integer; verbose=false) = x


function SymReduce(kd_in::KDHamProd; verbose=false)
    verbose && println(" ----- Symmetry reduce ----")
    verbose && println("  ham prod = ",KDshow(kd))
    kd = KDcopy(kd_in)
    kd.ham = SymReduce(kd.ham)
    return kd
end
    

function SymReduce(kd_in::KDHam; verbose=false)
    verbose && println(" ----- Symmetry reduce ----")
    verbose && println("  ham = ",KDshow(kd))
    kd=KDcopy(kd_in)
    if length(kd.IndxList) != 4
        return kd
    elseif kd.IndxList[1] > kd.IndxList[2]
        verbose && println("First pair wrong order")
        newkd = -kd ##Creates a copy
        newkd.IndxList[1:2]=[newkd.IndxList[2],newkd.IndxList[1]]
        verbose && println("revert  ham = ",KDshow(newkd))
        return SymReduce(newkd)
    elseif kd.IndxList[1] == kd.IndxList[2]
        verbose && println("First pair equal")
        return 0
    elseif kd.IndxList[3] > kd.IndxList[4]
        verbose && println("Second pair wrong order")
        newkd = -kd ##Creates a copy
        newkd.IndxList[3:4]=[newkd.IndxList[4],newkd.IndxList[3]]
        verbose && println("revert  ham = ",KDshow(newkd))
        return SymReduce(newkd)
    elseif kd.IndxList[3] == kd.IndxList[4]
        verbose && println("Second pair equal")
        return 0
    else
        return kd
    end
end


function SymReduce(kd_in::KDSumHamProd; verbose=false)
    verbose && println(" ----- Symmetry reduce ----")
    verbose && println("  kd = ",KDshow(kd))
    ###Check the special contition that
    ##Sum_{ij}H(i,j,k,l) = 0 = Sum_{kl}H(i,j,k,l)
    kd=KDcopy(kd_in)
    if kd.prod.KDList == [] && length(kd.ham.IndxList) == 4
        verbose && println("Special conditions fullfilled")
        ###Do deltas.. now check properties of the hamilronian
        if ((kd.ham.IndxList[1] in kd.IndxList) &&
            (kd.ham.IndxList[2] in kd.IndxList) &&
            !(kd.ham.IndxList[3] in kd.IndxList) &&
            !(kd.ham.IndxList[4] in kd.IndxList))
            ###By symmetry in this case the sum is zero
            verbose && println("1,2 in list, 3, 4 not")
            return 0
        elseif (!(kd.ham.IndxList[1] in kd.IndxList) &&
                 !(kd.ham.IndxList[2] in kd.IndxList) &&
                 (kd.ham.IndxList[3] in kd.IndxList) &&
                 (kd.ham.IndxList[4] in kd.IndxList))
            ###By symmetry in this case the sum is zero
            verbose && println("3,4 in list, 1, 2 not")
            return 0
            #error("Fix this")
        end
    end
    NewHam=SymReduce(kd.ham)
    if KDisZero(NewHam)
        return 0
    else
        kd.ham=NewHam
    end
    return  kd
end
        
function SymReduce(kd::KDHamProdSupPos; verbose=false)
    verbose && println(" ----- Symmetry reduce  KDHamProdSupPos ----")
    verbose && println("  kd = ",KDshow(kd))
    Tot = 0
    for indx in 1:length(kd.kdhpList)
        tmp1 = KDcopy(kd.kdhpList[indx])
        verbose && println("before tmp = ",KDshow(tmp1))
        tmp = SymReduce(tmp1)
        verbose && println("after  tmp = ",KDshow(tmp))
        Tot = Tot + tmp
        verbose && println("  Tot = ",KDshow(Tot))
    end
    return  Tot
end



function SymReduce(kd::KDSumHamProdSupPos; verbose=false)
    verbose && println(" ----- Symmetry reduce KDSumHamProdSupPos ----")
    verbose && println("  kd = ",KDshow(kd))
    Tot = 0
    for indx in 1:length(kd.kdshpList)
        verbose && println("  -,-,- [ $indx ] ,-,-,-,-,-,")
        tmp1 = KDcopy(kd.kdshpList[indx])
        verbose && println("before tmp1 = ",KDshow(tmp1))
        verbose && println("before Tot = ",KDshow(Tot))
        tmp = KDcopy(SymReduce(tmp1))
        verbose && println("after  tmp1 = ",KDshow(tmp1))
        verbose && println("after  tmp  = ",KDshow(tmp))
        verbose && println("after  Tot = ",KDshow(Tot))
        verbose && println("after  tmp  = ",tmp)
            Tot1 = 1*Tot + 1*tmp
        verbose && println("after  Tot1 = ",KDshow(Tot1))
        verbose && println("after  Tot = ",KDshow(Tot))
        Tot = KDcopy(Tot1)
        verbose && println("  Tot = ",KDshow(Tot))
    end
    return  Tot
end





## ------------------  Evaluation related scripts ----------


function MajFun(Left::Integer,Right::Integer,Center::Integer;
                Variant=nothing,FreeIndx=false)
    if Variant == nothing
        Variant = 1
    end
    if FreeIndx
        Var4="s_4"
        Var3="s_3"
        Var2="s_2"
        Var1="s_1"
    else
        Var4=4
        Var3=3
        Var2=2
        Var1=1
    end

    
    if Right == 0 && Left == 0
        if Center == 0
            return MajIP([])
        elseif Center == 2
            return MajIP(["i","j"])
        elseif Center == 4
            return MajIP(["i","j","l","k"])
        end
    elseif Right == 1 && Left == 1
        if Variant==1
            indx2=Var2
            indx1=Var1
        elseif Variant==2
            indx2=Var1
            indx1=Var1
        else
            error("Not impemented")
        end
        if Center == 0
            return MajIP([indx2,indx1])
        elseif Center == 2
            return -MajIP([indx2,"i",indx1,"j"])
        elseif Center == 4
            return MajIP([indx2,"i","j",indx1,"l","k"])
        end

    elseif Right == 2 && Left == 0
        if Center == 0
            return MajIP([Var1,Var2])
        elseif Center == 2
            return MajIP(["i",Var1,Var2,"j"])
        elseif Center == 4
            return MajIP(["i","j",Var1,Var2,"l","k"])
        end
    elseif Right == 0 && Left == 2
        if Center == 0
            return MajIP([Var4,Var3])
        elseif Center == 2
            return MajIP([Var4,Var3,"i","j"])
        elseif Center == 4
            return MajIP([Var4,Var3,"i","j","l","k"])
        end
    elseif Right == 2 && Left == 2
        if Variant==1
            indx4=Var4
            indx3=Var3
            indx2=Var2
            indx1=Var1
        elseif Variant==2
            indx4=Var2
            indx3=Var3
            indx2=Var3
            indx1=Var1
        elseif Variant==3
            indx4=Var1
            indx3=Var2
            indx2=Var2
            indx1=Var1
        else
            error("Not impemented")
        end
        if Center == 0
            return MajIP([indx4,indx3,indx2,indx1])
        elseif Center == 2
            return MajIP([indx4,indx3,"i",indx2,indx1,"j"])
        elseif Center == 4
            return MajIP([indx4,indx3,"i","j",indx2,indx1,"l","k"])
        else
            error("Not impemented")
        end
    elseif Right == 3 && Left == 1
        if Variant==1
            indx4=Var4
            indx3=Var3
            indx2=Var2
            indx1=Var1
        elseif Variant==2
            indx4=Var3
            indx3=Var3
            indx2=Var2
            indx1=Var1
        else
            error("Not impemented")
        end
        if Center == 0
            return MajIP([indx4,indx3,indx2,indx1])
        elseif Center == 2
            return -MajIP([indx4,"i",indx3,indx2,indx1,"j"])
        elseif Center == 4
            return MajIP([indx4,"i","j",indx3,indx2,indx1,"l","k"])
        else
            error("Not impemented")
        end
    elseif Right == 4 && Left == 0
        if Center == 0
            return MajIP([Var4,Var3,Var2,Var1])
        elseif Center == 2
            return MajIP([Var4,Var3,Var2,Var1,"i","j"])
        elseif Center == 4
            return MajIP(["i","j",Var4,Var3,Var2,Var1,"l","k"])
        end
    else
        error("Not implemented")
    end
end


function MajProd(Left::Integer,Right::Integer,Center::Integer;Variant=nothing,FreeIndx=false)
    if Center == 0
        Ham=KDHam()
    elseif Center == 2
        Ham=-KDHam("i","j")
    elseif Center == 4
        Ham=KDHam("i","j","k","l")
    else
        error("Not impemented")
    end
    return MajFun(Left,Right,Center;Variant=Variant,FreeIndx=FreeIndx)*Ham
end

function MajSum(Left::Integer,Right::Integer,Center::Integer;Variant=nothing,FreeIndx=false)
    Tmp=MajProd(Left,Right,Center;Variant=Variant,FreeIndx=FreeIndx)
    if Center == 0
        return KDSum(Tmp,[])
    elseif Center == 2
        return KDSum(Tmp,["i","j"])
    elseif Center == 4
        return KDSum(Tmp,["i","j","k","l"])
    else
        error("Not impemented")
    end
end

   
function MajCombo(Left::Integer,Center::Integer,Right::Integer;
                  Variant=nothing,FreeIndx=false)
    StrBraket="<$(Left)|$(Center)|$(Right)> = "
    if Variant==nothing
        VarStr=""
    else
        VarStr="$(Variant) "
    end
    println()
    MF = MajFun(Left,Right,Center, Variant=Variant,FreeIndx=FreeIndx)
    println(VarStr,"  ",StrBraket,KDshow(MF))
    println(VarStr,"  ",StrBraket,KDshow(MF,TeX=true))
    MS = MajSum(Left,Right,Center, Variant=Variant,FreeIndx=FreeIndx)
    println(VarStr," S",StrBraket,KDshow(MS))
    println(VarStr," S",StrBraket,KDshow(MS,TeX=true))
    MS = MajSum(Left,Right,Center, Variant=Variant,FreeIndx=FreeIndx)
    RMS = SymReduce(MS)
    println(VarStr,"RS",StrBraket,KDshow(RMS))
    println(VarStr,"RS",StrBraket,KDshow(RMS,TeX=true))
end


function MajTotal(Left::Integer,Right::Integer;Variant=nothing,FreeIndx=false)
    #println("Left:",Left)
    #println("Right:",Right)
    M0  = SymReduce(MajSum(Left,Right,0, Variant=Variant,FreeIndx=FreeIndx))
    M2  = SymReduce(MajSum(Left,Right,2, Variant=Variant,FreeIndx=FreeIndx))
    M4  = SymReduce(MajSum(Left,Right,4, Variant=Variant,FreeIndx=FreeIndx))
    #println("M0: ",M0)
    #println("M2: ",M2)
    #println("M4: ",M4)
    return M0 + M2 + M4
end
    







TabLevel=TabLevel[1:end-4]
println(TabLevel*"Close KD.jl")
