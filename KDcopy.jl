###Trivially reproduce integer
KDcopy(x::Integer)=x

function KDcopy(List::Array{KDIndex,1})
    return [KDcopy(x) for x in List]
end

function KDcopy(List::Array{KDs,1})
    return [KDcopy(x) for x in List]
end


function KDcopy(List::Array{KDHamProd})
    return [KDcopy(x) for x in List]
end

function KDcopy(List::Array{KD})
    return [KDcopy(x) for x in List]
end

function KDcopy(kd::KDSupPos)
    return KDSupPos(kd.x,sort!([KDcopy(x) for x in kd.kdpl]))
end



function KDcopy(kd::KDHamProdSupPos)
    return KDHamProdSupPos(sort!([KDcopy(x) for x in kd.kdhpList]))
end


function KDcopy(kd::KDSumHamProdSupPos)
    return KDSumHamProdSupPos(sort!([KDcopy(x) for x in kd.kdshpList]))
end



function KDcopy(kd::KDSumHamProd)
    return KDSumHamProd(KDcopy(kd.ham),KDcopy(kd.prod),KDcopy(kd.IndxList))
end


function KDcopy(kd::KDHamProd)
    return KDHamProd(KDcopy(kd.ham),KDcopy(kd.prod))
end

function KDcopy(kd::KDHam)
    return KDHam(kd.scale,KDcopy(kd.IndxList))
end

function KDcopy(kd::KDprod)
    return KDprod(KDcopy(kd.KDList),kd.scale)
end


function KDcopy(kd::KDIndex)
    return KDIndex(kd.x)
end

function KDcopy(kd::KD)
    return KD(KDcopy(kd.x),KDcopy(kd.y))
end
